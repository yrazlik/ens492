﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="coursequery.aspx.cs" Inherits="ENS492.coursequery"
    MasterPageFile="MasterPage.master" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
         <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table style="width: 84%;">
            <tr>
                <td class="style5" colspan="4">
                    <asp:Label ID="Label1" runat="server" 
                        Text="Please choose at least two courses from the dropdown lists below to check whether courses can have a time conflict" 
                        Font-Bold="True" ForeColor="#660066"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    <strong>Course 1
                </strong>
                </td>
                <td class="style6">
                    Course 2
                </td>
                <td class="style6">
                    Course 3
                </td>
                <td class="style4">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style3">
                
                    <asp:ComboBox ID="dropdown_course1" runat="server" AutoPostBack="False" DropDownStyle="DropDownList"
                        AutoCompleteMode="Suggest" CaseSensitive="False" ItemInsertLocation="Append" CssClass="CustomComboBoxStyle" >
                    </asp:ComboBox>
                </td>
                <td class="style3">
                    <asp:ComboBox ID="dropdown_course2" runat="server" AutoPostBack="False" DropDownStyle="DropDownList"
                        AutoCompleteMode="Suggest" CaseSensitive="False" ItemInsertLocation="Append" CssClass="CustomComboBoxStyle">
                    </asp:ComboBox>
                </td>
                <td class="style7">
                    <asp:ComboBox ID="dropdown_course3" runat="server" AutoPostBack="False" DropDownStyle="DropDownList"
                        AutoCompleteMode="Suggest" CaseSensitive="False" ItemInsertLocation="Append" CssClass="CustomComboBoxStyle">
                    </asp:ComboBox>
                </td>
                <td class="style3">
                    <asp:Button  ID="button_conflict_check_button" runat="server" OnClick="button_conflict_check_button_Click"
                        Text="Check" Width="90px" Height="28px" CssClass="buttons" />
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;
                </td>
                <td class="style3">
                    &nbsp;
                </td>
                <td class="style7">
                    &nbsp;
                </td>
                <td class="style3">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <asp:Label ID="label_status_label" runat="server"></asp:Label>
    <br />
    <br />
    <br />
    <asp:Table ID="table_visualize_blocks" runat="server">
        <asp:TableRow>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
            <asp:TableCell style="vertical-align:top;" Width="120"></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
   </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style3
        {
        }
        .style4
        {
            width: 22px;
            height: 23px;
        }
        .style5
        {
            height: 30px;
        }
        .style6
        {
            width: 22px;
            height: 23px;
            font-weight: bold;
        }
    .style7
    {
        width: 22px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">
        
            <p>
            <img src="Pictures/conflictcheck.jpg" alt="" style="height: 35px; width: 200px" />
            </p>
        
        </asp:Content>






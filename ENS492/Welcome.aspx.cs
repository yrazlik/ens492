﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ENS492
{
    public partial class Welcome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_show_weekly_schedule_Click(object sender, EventArgs e)
        {
            Response.Redirect("FilterByInstructor.aspx");
        }

        protected void linkbutton_check_conflict_Click(object sender, EventArgs e)
        {
            Response.Redirect("coursequery.aspx");
        }

        protected void linkbutton_create_new_academic_blocks_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewCoursesToBeOpened.aspx");
        }

        protected void button_show_academic_blocks_Click(object sender, EventArgs e)
        {
            Response.Redirect("AcademicBlockSearch.aspx");
        }

        protected void linkbutton_general_conflict_check_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShowWhichSetOFCoursesCanConflict.aspx");
        }

        protected void linkbutton_select_term_Click(object sender, EventArgs e)
        {
            Response.Redirect("SelectTerm.aspx");
        }

        protected void linkbutton_upload_file_Click(object sender, EventArgs e)
        {
            Response.Redirect("FileUploader.aspx");
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popupdeneme.aspx.cs" Inherits="ENS492.popupdeneme" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Button ID="Button2" runat="server" Text="Button" onclick="Button2_Click" />

        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" CancelControlID="btnCancel"
            OkControlID="btnOkay" TargetControlID="Button1" PopupControlID="Panel1" PopupDragHandleControlID="PopupHeader"
            Drag="true" ></asp:ModalPopupExtender>
        


        <asp:Panel ID="Panel1" Style="display: none" runat="server">
            <div class="HellowWorldPopup">
                <div class="PopupHeader" id="PopupHeader">
                    Header</div>
                <div class="PopupBody">
                    <p>
                        This is a simple modal dialog</p>
                </div>
                <div class="Controls">
                    <input id="btnOkay" type="button" value="Done" />
                    <input id="btnCancel" type="button" value="Cancel" />
                </div>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ENS492.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 484px;
            text-align: right;
        }
        .style3
        {
            width: 256px;
        }
        .style4
        {
            width: 484px;
            height: 23px;
            text-align: right;
        }
        .style5
        {
            width: 256px;
            height: 23px;
        }
        .style6
        {
            height: 23px;
        }
        .style7
        {
            width: 484px;
            text-align: right;
            height: 26px;
        }
        .style8
        {
            width: 256px;
            height: 26px;
        }
        .style9
        {
            height: 26px;
        }
        .style10
        {
            width: 484px;
            text-align: right;
            height: 34px;
        }
        .style11
        {
            width: 256px;
            height: 34px;
        }
        .style12
        {
            height: 34px;
        }
        .style13
        {
            text-align: right;
            height: 30px;
        }
        .style15
        {
            height: 30px;
        }
        .style16
        {
            font-size: small;
        }
        .style17
        {
            width: 256px;
            height: 23px;
            color: #FF0000;
        }
        </style>
</head>
<body>


    <form id="form1" runat="server">
    <div style="height: 242px; width: 1081px">
    
        <span style="color: rgb(48, 54, 232); font-family: Verdana, 'Arial Narrow', helvetica, sans-serif; font-size: 20pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: right; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">
        <br />
        <br />
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="Pictures/security.jpg" alt="" />INFORMATION SYSTEM LOGIN</strong><br />
        <br />
        <br />
        <br />
        <br />
        </span>
    
    </div>
    <table class="style1">
        <tr>
            <td class="style13" colspan="2">
    
        <span style="color: rgb(255, 9, 9); font-family: Verdana, 'Arial Narrow', helvetica, sans-serif; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;" 
                    class="style16">
                <strong>Enter your ID and password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                </strong>
        </span></td>
            <td class="style15">
                </td>
        </tr>
        <tr>
            <td class="style2">
                User ID:</td>
            <td class="style3">
                <asp:TextBox ID="idBox" runat="server" Width="160px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="*You must enter your user ID" style="color: #FF0000" 
                    ControlToValidate="idBox"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style7">
                Password:</td>
            <td class="style8">
                <asp:TextBox  ID="passwordBox" runat="server" Width="160px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="style9">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="passwordBox" ErrorMessage="*You must enter your password" 
                    style="color: #FF0000"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td class="style10">
                Login As:</td>
            <td class="style11">
                <asp:DropDownList ID="LoginAs" runat="server" Height="23px" 
                    onselectedindexchanged="LoginAs_SelectedIndexChanged" Width="160px">
                    <asp:ListItem>Please Choose...</asp:ListItem>
                    <asp:ListItem>Student</asp:ListItem>
                    <asp:ListItem>Instructor</asp:ListItem>
                    <asp:ListItem>Administrator</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="LoginAs" ErrorMessage="*You must select one" 
                    InitialValue="Please Choose..." style="color: #FF0000"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style5">
                <asp:Button ID="loginButton" runat="server" Text="Login" Width="160px" 
                    onclick="loginButton_Click" />
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style17">
                <asp:Label ID="LoginError" runat="server" Text="Please check the inputs again" 
                    Visible="False"></asp:Label>
                <br />
                <asp:Label ID="ldisabled" runat="server" 
                    Text="Your account is disabled or deleted. Contact bannerweb@sabanciuniv.edu" 
                    Visible="False"></asp:Label>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
    </table>
    </form>
    </body>
</html>

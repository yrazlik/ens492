﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENS492.Classes;
using System.Drawing;

namespace ENS492
{
    public partial class SelectTerm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                String term = TextBox1.Text.ToString();
                CommonFunctions.currentTerm = term;

                int year = Convert.ToInt32(term.Substring(0, 4));
                string semester = term.Substring(4, 2);

                CommonFunctions.prevYearSameTerm = (year - 1).ToString() + semester;

                if (semester.Substring(1, 1) == "1")
                {
                    semester = "02";
                    year--;
                }
                else if (semester.Substring(1, 1) == "2")
                {
                    semester = "01";
                }

                
                CommonFunctions.prevTerm = year.ToString() + semester;
                Label1.Text = "Term has been changed succesfully";
                Label1.ForeColor = Color.Green;
                Label1.Visible = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Invalid term. Term should be in such a format: Year followed by term. For example 201301, 201302.";
                Label1.ForeColor = Color.Red;
                Label1.Visible = true;
            }

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcademicBlockSearch.aspx.cs"
    Inherits="ENS492.AcademicBlockSearch" MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <table style="width: 84%;">
                <tr>
                    <td class="style4" colspan="5">
                        <asp:Label ID="lbl_message" runat="server" Font-Bold="False" ForeColor="Red" 
                            Text="You need to select ONE search option below."></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td class="style5">
                        &nbsp;<asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Program: "></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Height="16px" 
                            oncheckedchanged="CheckBox1_CheckedChanged" Width="20px" />
                    </td>
                    <td class="style3">
                        <ajaxToolkit:ComboBox CssClass="" ID="combobox_program" runat="server" DropDownStyle="DropDownList"
                            AutoCompleteMode="Suggest"  DataTextField="fullname"
                            DataValueField="fullname" MaxLength="0" Style="display: inline;">                            
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem>CS-Computer Science and EngineeringBSCS</asp:ListItem>
                            <asp:ListItem>CULT-Cultural Studies</asp:ListItem>
                            <asp:ListItem>ECON-EconomicsBAECON</asp:ListItem>
                            <asp:ListItem>IS-International Studies</asp:ListItem>
                            <asp:ListItem>MGMT-Management</asp:ListItem>
                            <asp:ListItem>SPS-Social and Political Sciences</asp:ListItem>
                            <asp:ListItem>VA-Visual Arts and Visual Communications Design</asp:ListItem>
                            <asp:ListItem>BIO-Biological Sciences and Bioengineering</asp:ListItem>
                            <asp:ListItem>EE-Electronics Engineering</asp:ListItem>
                            <asp:ListItem>EL-Microelectronics</asp:ListItem>
                            <asp:ListItem>MAT-Materials Science and  Engineering</asp:ListItem>
                            <asp:ListItem>ME-Mechatronics</asp:ListItem>
                            <asp:ListItem>MS-Manufacturing Systems Engineering</asp:ListItem>
                            <asp:ListItem>TE-TelecommunicationsBSTE</asp:ListItem>
                        </ajaxToolkit:ComboBox>
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        &nbsp;<asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Course: "></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" Height="16px" 
                            oncheckedchanged="CheckBox2_CheckedChanged" Width="20px" />
                    </td>
                    <td class="style3">
                        <ajaxToolkit:ComboBox CssClass="" ID="dropdown_course" runat="server" DropDownStyle="DropDownList"
                            AutoCompleteMode="Suggest" DataSourceID="SqlDataSource2" DataTextField="fullname"
                            DataValueField="fullname" MaxLength="0" Style="display: inline;">
                            
                        </ajaxToolkit:ComboBox>
                        
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT subj+' ' + numb+'-' + title as [fullname] FROM Courses ORDER BY fullname


"></asp:SqlDataSource>
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        &nbsp;<asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Instructor: "></asp:Label>
                        &nbsp;<asp:CheckBox ID="CheckBox3" runat="server" AutoPostBack="True" 
                            Height="16px" oncheckedchanged="CheckBox3_CheckedChanged" Width="20px" />
                    </td>
                    <td class="style3">
                        <ajaxToolkit:ComboBox CssClass="" ID="dropdown_instructor" runat="server" DropDownStyle="DropDownList"
                            AutoCompleteMode="Suggest" DataSourceID="SqlDataSource1" DataTextField="name"
                            DataValueField="name" MaxLength="0" Style="display: inline;">
                        </ajaxToolkit:ComboBox>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [name] FROM [Instructors] ORDER BY [name]"></asp:SqlDataSource>
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        <asp:Button ID="button_submit" runat="server" OnClick="button_submit_Click" Text="Submit"
                            Width="80px" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:Table ID="table_visualize_blocks" runat="server">
                <asp:TableRow>
                    <asp:TableCell Width="80" style="vertical-align:top;" ></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                    <asp:TableCell Width="80" style="vertical-align:top;"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style3
        {
        }
        .style4
        {
            height: 34px;
        }
        .style5
        {
            width: 203px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
    <p>
        <img src="Pictures/sabanci_main_menu.gif" alt="" style="height: 35px; width: 200px" />
    </p>
</asp:Content>

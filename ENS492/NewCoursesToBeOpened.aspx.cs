﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ENS492.Classes;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;

namespace ENS492
{
    class LinkColumn : ITemplate
    {
        public void InstantiateIn(System.Web.UI.Control container)
        {
            LinkButton link = new LinkButton();
            link.ID = "delete";
            container.Controls.Add(link);
        }
    }

    public partial class NewCoursesToBeOpened : System.Web.UI.Page
    {
        static HashSet<string> prevYearCourses, thisYearCourses;
        static List<string> coursesToRemove;
        static List<AcademicBlock> prevYearAcademicBlocks;
        static string courseBeingAdded;
        static List<String> coursesToAdd = new List<string>();
        static List<AcademicBlock> allAcademicBlocks;
        static List<BlockCourse> staticCoursesThatShouldNotConflictWithSelectedCourse;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void button_upload_Click(object sender, EventArgs e)
        {
            if (uploader.HasFile)
            {
                try
                {
                    thisYearCourses = new HashSet<string>();
                    prevYearCourses = new HashSet<string>();
                    string filename = Path.GetFileName(uploader.FileName);
                    uploader.SaveAs(Server.MapPath("~/") + filename);

                    InputFile inputFile = new InputFile(filename, Server.MapPath("~/") + filename);
                    FileToDBAdapter adapter = new FileToDBAdapter(inputFile);








                    var reader = new StreamReader(File.OpenRead(inputFile.getFilePath()));

                    try
                    {
                        CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                        CommonFunctions.con.Open();
                    }
                    catch (Exception ex)
                    {
                    }
                    SqlCommand cmd;
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var values = line.Split(';');

                        string coursecode = values[0];
                        string coursenumber = values[1];

                        string course = coursecode + ";" + coursenumber;
                        thisYearCourses.Add(course);
                    }


                    cmd = new SqlCommand("SELECT * FROM Courses where term=@term", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.prevYearSameTerm);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow row in dt.Rows)
                    {
                        string subj = row["subj"].ToString();
                        string numb = row["numb"].ToString();
                        string course = subj + ";" + numb;

                        prevYearCourses.Add(course);
                    }

                    IEnumerable<string> coursesToBeRemoved = prevYearCourses.Except(thisYearCourses);

                    DataTable table = new DataTable();
                    if (table.Columns.Count == 0)
                    {
                        table.Columns.Add("CourseCode", typeof(string));
                        table.Columns.Add("CourseNumber", typeof(string));
                    }

                    foreach (string s in coursesToBeRemoved)
                    {
                        var values = s.Split(';');
                        string code = values[0], number = values[1];
                        DataRow NewRow = table.NewRow();
                        NewRow[0] = code;
                        NewRow[1] = number;
                        table.Rows.Add(NewRow);
                    }
                    showCourses.DataSource = table;
                    showCourses.DataBind();

                }

                catch (Exception ex)
                {
                }
                button_submit_changes.Visible = true;
                lbl_remove.Visible=true;
            }
        }

        protected void button_submit_changes_Click(object sender, EventArgs e)
        {
            coursesToRemove = new List<string>();
            prevYearAcademicBlocks = new List<AcademicBlock>();
            for (int i = 0; i < showCourses.Rows.Count; i++)
            {
                bool selected = ((CheckBox)showCourses.Rows[i].FindControl("checkbox")).Checked;
                if (selected)
                {
                    string courseCode = showCourses.Rows[i].Cells[1].Text, courseNumber = showCourses.Rows[i].Cells[2].Text;
                    coursesToRemove.Add(courseCode + " " + courseNumber);
                }
            }

            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {
            }

            SqlCommand cmd;
            cmd = new SqlCommand("SELECT DISTINCT blockname FROM academicblocks where term=@term", CommonFunctions.con);
            cmd.Parameters.AddWithValue("@term", CommonFunctions.prevYearSameTerm);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows) //get all the academic blocks of the previous term
            {
                string blockName = row["blockname"].ToString();
                AcademicBlock a = new AcademicBlock(blockName);
                prevYearAcademicBlocks.Add(a);
            }

            foreach (AcademicBlock block in prevYearAcademicBlocks)
            {
                if (block.blockCourses == null)
                    block.blockCourses = new List<BlockCourse>();

                SqlCommand com;
                com = new SqlCommand("SELECT coursename, coursesection FROM academicblocks where term=@term and blockname=@blockname", CommonFunctions.con);
                com.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
                com.Parameters.AddWithValue("@blockname", block.getBlockName());

                SqlDataAdapter da2 = new SqlDataAdapter(com);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                foreach (DataRow row in dt2.Rows) //get all courses of all academic blocks
                {
                    string course = row["coursename"].ToString();
                    string section = row["coursesection"].ToString();
                    block.blockCourses.Add(new BlockCourse(course, section));
                }//now we have all academic blocks of previous term and the courses of these blocks
            }

            foreach (string s in coursesToRemove)
            {
                for (int i = 0; i < prevYearAcademicBlocks.Count; i++)
                {
                    for (int j = 0; j < prevYearAcademicBlocks[i].blockCourses.Count; j++)
                    {
                        if (prevYearAcademicBlocks[i].blockCourses[j].getCourseName() == s)
                        {
                            prevYearAcademicBlocks[i].blockCourses.RemoveAt(j);
                        }
                    }
                }
            }//the selected courses were removed

            /*  StreamWriter writer = new StreamWriter("C:\\NewAcademicBlocks.txt");
              foreach (AcademicBlock b in academicBlocks)
              {
                  writer.WriteLine(b.getBlockName() + ";");
                  foreach (BlockCourse blc in b.blockCourses)
                  {
                      writer.WriteLine(blc.getCourseName() + ";" + blc.getCourseSection() + ";");
                  }
                  writer.WriteLine();
              }
              writer.Close();*/

            IEnumerable<string> coursesToBeAdded = thisYearCourses.Except(prevYearCourses);

            DataTable table = new DataTable();
            if (table.Columns.Count == 0)
            {
                table.Columns.Add("CourseCode", typeof(string));
                table.Columns.Add("CourseNumber", typeof(string));
            }

            foreach (string s in coursesToBeAdded)
            {
                var values = s.Split(';');
                string code = values[0], number = values[1];
                DataRow NewRow = table.NewRow();
                NewRow[0] = code;
                NewRow[1] = number;
                table.Rows.Add(NewRow);
            }
            showCoursesToBeAdded.DataSource = table;
            showCoursesToBeAdded.DataBind();
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            /*  Console.WriteLine("");
              int index = Convert.ToInt32(e.CommandArgument);
              GridViewRow row = showCoursesToBeAdded.Rows[index];
              AjaxControlToolkit.ModalPopupExtender btnPopUp_ModalPopupExtender = (AjaxControlToolkit.ModalPopupExtender)row.FindControl("btnPopUp_ModalPopupExtender");
              btnPopUp_ModalPopupExtender.Show();*/
        }

        public bool blockContainsThatCourse(AcademicBlock a, string course)
        {
            foreach (BlockCourse b in a.blockCourses)
            {
                if (b.getCourseName() == course)
                    return true;
            }

            return false;

        }

        public bool blockAlreadyExists(List<AcademicBlock> blockList, AcademicBlock academicBlock)
        {
            foreach (AcademicBlock a in blockList)
            {
                if (a.getBlockName() == academicBlock.getBlockName())
                    return true;
            }

            return false;

        }

        protected void showCoursesToBeAdded_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.CompareTo("ShowSuggestion") == 0)
            {
                blockSuggestions.Visible = true;
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = showCoursesToBeAdded.Rows[index];
                AjaxControlToolkit.ModalPopupExtender btnPopUp_ModalPopupExtender = (AjaxControlToolkit.ModalPopupExtender)row.FindControl("btnPopUp_ModalPopupExtender");
                //   btnPopUp_ModalPopupExtender.Show();
                TextBox box = (TextBox)row.FindControl("TextBox1");
                string courseName = row.Cells[0].Text + " " + row.Cells[1].Text;
                string similarCourse = row.Cells[0].Text + " " + row.Cells[1].Text.Substring(0, 1);
                courseBeingAdded = courseName;

                //now find all academic blocks that contains this coursename and show them in popup

                try
                {
                    CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                    CommonFunctions.con.Open();
                }
                catch (Exception ex)
                {
                }

                List<AcademicBlock> academicBlocksThatContainSimilarCourse = new List<AcademicBlock>();
                List<BlockCourse> coursesThatShouldNotConflictWithSelectedCourse = new List<BlockCourse>();
                

                foreach (AcademicBlock a in prevYearAcademicBlocks)
                {
                    if (blockContainsThatCourse(a, courseName))
                    {
                        academicBlocksThatContainSimilarCourse.Add(new AcademicBlock(a.getBlockName()));
                        foreach (BlockCourse b in a.blockCourses)
                        {
                            if (b.getCourseName() != courseName)
                            {
                                coursesThatShouldNotConflictWithSelectedCourse.Add(new BlockCourse(b.getCourseName(), b.getCourseSection()));
                            }
                        }
                    }
                }// now we have all courses that should not conflict with the course that we will take suggestion(yani mesela cs308 için öneri alcaz, cs308 ile çakışmaması gereken tüm dersler burada)

                staticCoursesThatShouldNotConflictWithSelectedCourse = coursesThatShouldNotConflictWithSelectedCourse;


                foreach (BlockCourse b in coursesThatShouldNotConflictWithSelectedCourse)
                {
                    SqlCommand commnd = new SqlCommand("SELECT DISTINCT blockname from academicblocks where coursename=@coursename", CommonFunctions.con);
                    commnd.Parameters.AddWithValue("@coursename", b.getCourseName());

                    SqlDataAdapter dadp = new SqlDataAdapter(commnd);
                    DataTable tabl = new DataTable();
                    dadp.Fill(tabl);

                    foreach (DataRow r in tabl.Rows)
                    {
                        string block = r["blockname"].ToString();
                        AcademicBlock a = new AcademicBlock(block);
                        if (!blockAlreadyExists(academicBlocksThatContainSimilarCourse,a))
                            academicBlocksThatContainSimilarCourse.Add(a);
                    }


                }

                

            /*    SqlCommand cmd; //get all the academic blocks that contains that course
                cmd = new SqlCommand("SELECT DISTINCT blockname FROM academicblocks where coursename=@courseName", CommonFunctions.con);
                cmd.Parameters.AddWithValue("@coursename", courseName);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    string blockName = dr["blockname"].ToString();
                    AcademicBlock a = new AcademicBlock(blockName);
                    academicBlocksThatContainSimilarCourse.Add(a);
                }*/

                SqlCommand command = new SqlCommand("SELECT DISTINCT blockname FROM academicblocks where coursename LIKE '%' + @similarCourse + '%'", CommonFunctions.con);
                command.Parameters.AddWithValue("@similarCourse", similarCourse);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable datatable = new DataTable();
                adapter.Fill(datatable);

                foreach (DataRow dr in datatable.Rows)
                {
                    string blockName = dr["blockname"].ToString();
                    AcademicBlock a = new AcademicBlock(blockName);
                    if (!blockAlreadyExists(academicBlocksThatContainSimilarCourse, a))
                        academicBlocksThatContainSimilarCourse.Add(a);
                }


                foreach (AcademicBlock block in academicBlocksThatContainSimilarCourse)
                {
                    if (block.blockCourses == null)
                        block.blockCourses = new List<BlockCourse>();

                    SqlCommand com;
                    com = new SqlCommand("SELECT coursename, coursesection FROM academicblocks where term=@term and blockname=@blockname", CommonFunctions.con);
                    com.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); 
                    com.Parameters.AddWithValue("@blockname", block.getBlockName());

                    SqlDataAdapter da2 = new SqlDataAdapter(com);
                    DataTable dt2 = new DataTable();
                    da2.Fill(dt2);

                    foreach (DataRow dr in dt2.Rows) //get all courses of all academic blocks
                    {
                        string course = dr["coursename"].ToString();
                        string section = dr["coursesection"].ToString();
                        block.blockCourses.Add(new BlockCourse(course, section));
                    }//now we have all academic blocks that contains similar course (e.g. CS3xx) and the courses of these blocks
                }


                //now fill the tables in modalpopup



                /*
                Table tableVisual = table_visualize_blocks;

                
                
                int Index = 0;
                foreach (AcademicBlock ab in academicBlocksThatContainThatCourse)
                {
                    var linkField = new TemplateField();
                    linkField.ItemTemplate = new LinkColumn();
                    
                    

                    GridView grid = new GridView();
                    grid.AutoGenerateColumns=true;
                    grid.RowStyle.CssClass = "rowstyle";
                    grid.HeaderStyle.CssClass = "headerstyle";
                    grid.AlternatingRowStyle.CssClass = "altrowstyle";
                    grid.BackColor = Color.White;
                    grid.BorderColor=Color.White;
                    grid.BorderStyle=BorderStyle.None;
                    grid.BorderWidth=1;
                    grid.CellPadding=4;
                    grid.ForeColor=Color.Black;
                    grid.GridLines=GridLines.Vertical;
                    grid.ID = "_gridview" + Index;

                   // grid.Columns.Add(linkField);
                   
                    DataTable table = new DataTable();
                    if (table.Columns.Count == 0)
                    {
                        table.Columns.Add("BlockCourses", typeof(string));

                    }

                   
                    foreach (BlockCourse bc in ab.blockCourses)
                    {
                        DataRow NewRow = table.NewRow();
                        
                        NewRow[0] += bc.getCourseName() + " " + bc.getCourseSection();
                        table.Rows.Add(NewRow);
                    }
                    grid.DataSource = table;
                  //  ButtonField bf = new ButtonField();
                 //   bf.Text="Remove?";
                 //   bf.ButtonType = ButtonType.Link;
                //    grid.Columns.Add(bf);
                    grid.DataBind();
                    
                    grid.HeaderRow.Cells[0].Text = ab.getBlockName();
                    
                    tableVisual.Rows[0].Cells[Index].Controls.Add(grid);
                    Index++;
                }


                */

                
                blockSuggestions.DataSource = null;
                blockSuggestions.DataBind();

                DataTable table = new DataTable();
                if (table.Columns.Count == 0)
                {
                    table.Columns.Add("BlockName", typeof(string));
                    table.Columns.Add("BlockCourses", typeof(string));
                }


                foreach (AcademicBlock ab in academicBlocksThatContainSimilarCourse)
                {
                    DataRow NewRow = table.NewRow();
                    NewRow[0] = ab.getBlockName();
                    string coursesInThatBlock = "";
                    foreach (BlockCourse bc in ab.blockCourses)
                    {
                        coursesInThatBlock += bc.getCourseName() + " " + bc.getCourseSection() + ", ";
                    }

                    coursesInThatBlock = coursesInThatBlock.Substring(0, coursesInThatBlock.Length - 2);

                    NewRow[1] = coursesInThatBlock;
                    table.Rows.Add(NewRow);
                }


                blockSuggestions.DataSource = table;
                blockSuggestions.DataBind();
                button_done_adding.Visible = true;
                button_done_adding.Enabled = true;
                button_finish_editing.Visible = true;
                Label1.Visible = true;
                TextBox1.Visible = true;

                if (academicBlocksThatContainSimilarCourse.Count == 0)
                {
                    button_done_adding.Enabled = false;
                    lbl_message.Text = "Sorry, no suitable suggestions found";
                    lbl_message.Visible = true;
                    lbl_message.ForeColor = Color.Red;
                }
                else
                {
                    button_done_adding.Enabled = true;
                    lbl_message.Visible = false;
                }

            }




        }

        protected void button_done_adding_Click(object sender, EventArgs e)
        {
            
            for (int i = 0; i < blockSuggestions.Rows.Count; i++)
            {
                bool selected = ((CheckBox)blockSuggestions.Rows[i].FindControl("checkboxAdd")).Checked;
                if (selected)
                {
                    string blockName = blockSuggestions.Rows[i].Cells[1].Text;
                    coursesToAdd.Add(courseBeingAdded + ";" + blockName);
                }
            }
            if (TextBox1.Text.ToString().Length > 0)
            {
                AcademicBlock a = new AcademicBlock(TextBox1.Text.ToString());
                a.blockCourses = new List<BlockCourse>();
                foreach (BlockCourse b in staticCoursesThatShouldNotConflictWithSelectedCourse)
                {
                    a.blockCourses.Add(b);
                }

                prevYearAcademicBlocks.Add(a);
            }

            lbl_message.Visible = true;
            lbl_message.Text = "Course Added To Selected Blocks Succesfully";
            lbl_message.ForeColor = Color.Green;
            blockSuggestions.Visible = false;
            button_done_adding.Visible=false;
            Label1.Visible = false;
            TextBox1.Visible = false;
        }

        protected void button_finish_editing_Click(object sender, EventArgs e)
        {
            foreach (string s in coursesToAdd)
            {
                var values = s.Split(';');
                string course = values[0], blockNameToBeAdded = values[1];
                bool contains = false;

                for (int i = 0; i < prevYearAcademicBlocks.Count; i++)
                {
                   /* for (int j = 0; j < prevYearAcademicBlocks[i].blockCourses.Count; j++)
                    {
                        if (prevYearAcademicBlocks[i].blockCourses[j].getCourseName() == course)
                        {
                            contains = true;
                        }
                    }*/

                   // if (!contains)
                  //  {
                    if(prevYearAcademicBlocks[i].getBlockName()==blockNameToBeAdded)
                        prevYearAcademicBlocks[i].blockCourses.Add(new BlockCourse(course));
                   // }
                }


            }

              StreamWriter writer = new StreamWriter("C:\\NewAcademicBlocks.txt");
              foreach (AcademicBlock b in prevYearAcademicBlocks)
              {
                  writer.WriteLine(b.getBlockName() + ";");
                  foreach (BlockCourse blc in b.blockCourses)
                  {
                      writer.WriteLine(blc.getCourseName() + ";" + blc.getCourseSection() + ";");
                  }
                  writer.WriteLine();
              }
              writer.Close();
              lbl_finish.Visible = true;
        }
    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENS492.Classes;
using System.Data.SqlClient;
using System.Data;

namespace ENS492
{
    public partial class AcademicBlockSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void button_submit_Click(object sender, EventArgs e)
        {
            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {
            }

            string  program = combobox_program.SelectedValue.ToString(), instructor=dropdown_instructor.SelectedValue.ToString(), course=dropdown_course.SelectedValue.ToString();

            
                
            
            if (program != "" && combobox_program.Enabled)
            {
                var values = program.Split('-');
                string courseCode = values[0];

                //term şimdilik
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT [blockname] FROM academicblocks WHERE term=@term", CommonFunctions.con);
                cmd.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<string> blocks1 = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    string block = row["blockname"].ToString();
                    if (!blocks1.Contains(block))
                    {
                        blocks1.Add(block);
                    }
                }

                List<string> blocksToVisualize = new List<string>();
                foreach (string s in blocks1)
                {
                    if (s.Contains(courseCode))
                    {
                        blocksToVisualize.Add(s);
                    }
                }

                visualizeBlocks(blocksToVisualize);
            }
            else if (course != "" && dropdown_course.Enabled)
            {
                var values = course.Split('-');
                string courseToSearch = values[0];
                
                //term şimdilik
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT [blockname] FROM academicblocks where coursename=@coursename AND term=@term", CommonFunctions.con);
                cmd.Parameters.AddWithValue("@coursename", courseToSearch);
                cmd.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<string> blocks1 = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    string blockName = row["blockname"].ToString();
                    if (!blocks1.Contains(blockName))
                    {
                        blocks1.Add(blockName);
                    }
                }



                visualizeBlocks(blocks1);
            }
            else if (instructor != "" && dropdown_instructor.Enabled)
            {
                
                SqlCommand com = new SqlCommand("SELECT subj, numb, section FROM Courses WHERE instructor=@instructor AND term=@term", CommonFunctions.con);
                com.Parameters.AddWithValue("@instructor", instructor);
                com.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter daa = new SqlDataAdapter(com);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                List<string> coursesOfThatInstructor = new List<string>();
                foreach (DataRow row in dtt.Rows)
                {
                    string subj = row["subj"].ToString(), numb = row["numb"].ToString(), section = row["section"].ToString();
                    string crs="";
                    if (section != "0")
                    {
                        crs = subj + " " + numb + section;
                    }
                    else
                    {
                        crs = subj + " " + numb;
                    }
                    if (!coursesOfThatInstructor.Contains(crs))
                    {
                        coursesOfThatInstructor.Add(crs);
                    }
                }

                List<String> blocks1 = new List<string>();
                foreach (string s in coursesOfThatInstructor)
                {
                    //term şimdilik
                    SqlCommand cmd = new SqlCommand("SELECT DISTINCT [blockname] FROM academicblocks where coursename=@coursename AND term=@term", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@coursename", s);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                    {
                        string blockName = row["blockname"].ToString();
                        if (!blocks1.Contains(blockName))
                        {
                            blocks1.Add(blockName);
                        }
                    }
                }

                visualizeBlocks(blocks1);
            }


            
        }

        public void visualizeBlocks(List<String> intersectedBlocks)
        {
            Table tableVisual = table_visualize_blocks;
            //  tableVisual.Rows.Clear();
            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();

            }
            catch (Exception ex)
            {
            }
            int index = 0;

            foreach (String blockName in intersectedBlocks)
            {
                try
                {
                    DataTable subjects = new DataTable();

                    DataTable dt = new DataTable();

                    SqlCommand cmd = new SqlCommand("SELECT [coursename], [coursesection] FROM academicblocks where blockname=@blockname AND term=@term", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@blockname", blockName);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    Table t = new Table();
                    t.CssClass = "singletable";
                    //    t.Attributes.Add("CssClass", "singletable");
                    t.BorderWidth = 1;
                    //    t.GridLines = GridLines.Horizontal;

                    TableRow row = new TableRow();
                    TableCell cl = new TableCell();
                    //     cl.BackColor=Color.FromArgb(240,232,146);
                    cl.Text = blockName;
                    row.Cells.Add(cl);
                    t.Rows.Add(row);




                    foreach (DataRow r in dt.Rows)
                    {
                        TableRow tr = new TableRow();
                        TableCell c = new TableCell();
                        c.Text = r["coursename"] + " " + r["coursesection"];
                        //      c.BackColor = Color.FromArgb(161, 240, 204);
                        tr.Cells.Add(c);
                        t.Rows.Add(tr);
                    }

                    tableVisual.Rows[0].Cells[index].Controls.Add(t);

                }
                catch (Exception e)
                {
                    CommonFunctions.con.Close();
                }
                index++;
                CommonFunctions.con.Close();

            }

        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox1.Checked)
            {
                CheckBox2.Checked = false;
                CheckBox3.Checked = false;
                dropdown_course.Enabled = false;
                dropdown_instructor.Enabled = false;
                combobox_program.Enabled = true;
            }
            
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {

            if (CheckBox2.Checked)
            {
                CheckBox1.Checked = false;
                CheckBox3.Checked = false;
                dropdown_course.Enabled = true;
                dropdown_instructor.Enabled = false;
                combobox_program.Enabled = false;
            }
        }

        protected void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {

            if (CheckBox3.Checked)
            {
                CheckBox2.Checked = false;
                CheckBox1.Checked = false;
                dropdown_course.Enabled = false;
                dropdown_instructor.Enabled = true;
                combobox_program.Enabled = false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENS492.Classes;
using System.Data.SqlClient;
using System.Data;

namespace ENS492
{
    public partial class ShowWhichSetOFCoursesCanConflict : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dropdown_first_set_of_courses.Items.Insert(0, "Select One...");
            dropdown_first_set_of_course_numbers.Items.Insert(0, "Select One...");
            dropdown_second_set_of_courses.Items.Insert(0, "Select One...");
            dropdown_second_set_of_course_numbers.Items.Insert(0, "Select One...");
            dropdown_third_set_of_courses.Items.Insert(0, "Select One...");
            dropdown_third_set_of_course_numbers.Items.Insert(0, "Select One...");

            label_error.Visible = false;
            label_courses_that_can_conflict.Visible = false;
        }



        private List<string> findCoursesThatCanConflict(string subj1, string numb1, string subj2, string numb2)
        {
            List<Course> setOfFirstCourses = new List<Course>();
            List<Course> setOfSecondCourses = new List<Course>();
            List<Course> allCoursesInThatTerm = new List<Course>();
            List<string> coursesThatCanConflict = new List<string>();

            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {
            }

            //Query for first set of courses, first get all courses from Timetable for that term
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT * FROM Timetable WHERE term=@term", CommonFunctions.con);
            cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            
            // here, get the CS 3XX courses for example
            foreach (DataRow row in dt.Rows)
            {
                Course c = new Course(row["subj"].ToString(), row["numb"].ToString(), row["section"].ToString(), row["starthour"].ToString(), row["endhour"].ToString(), row["day"].ToString(), row["building"].ToString(), row["room"].ToString());
                if (c.getSubj() == subj1 && c.getNumb().Substring(0, 1) == numb1.Substring(0, 1))
                {
                    bool exists = false;
                    foreach (Course course in setOfFirstCourses)
                    {
                        if (c.getCourseName() == course.getCourseName())
                        {
                            exists = true;
                        }
                    }
                    if (!exists)
                    {
                        setOfFirstCourses.Add(c);
                    }
                }
                if (c.getSubj() == subj2 && c.getNumb().Substring(0, 1) == numb2.Substring(0, 1))
                {
                    bool exists = false;

                    foreach (Course course in setOfSecondCourses)
                    {
                        if (c.getCourseName() == course.getCourseName())
                        {
                            exists = true;
                        }
                    }
                    if (!exists)
                    {
                        setOfSecondCourses.Add(c);
                    }
                }

            } //now we have the first and second sets of courses, now for each pair check the academic blocks


            

            foreach (Course c1 in setOfFirstCourses)
            {
                string firstCourse = c1.getSubj() + " " + c1.getNumb();
                foreach (Course c2 in setOfSecondCourses)
                {
                    if (c1 != c2)
                    {
                        string secondCourse = c2.getSubj() + " " + c2.getNumb();
                        List<string> firstBlocks = new List<string>(), secondBlocks = new List<string>();

                        //get the academic blocks of c1 and c2, if they do not have any academic block in common, that means they can conflict
                        SqlCommand cmd2 = new SqlCommand("SELECT blockname FROM academicblocks WHERE term=@term and coursename=@coursename", CommonFunctions.con);
                        cmd2.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); // şimdilik, düzelt: şu anki data böyle olduğundan term prevterm burada, normali term=currentterm olmalı
                        cmd2.Parameters.AddWithValue("@coursename", firstCourse);
                        SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                        DataTable dt2 = new DataTable();
                        da2.Fill(dt2);

                        foreach (DataRow row in dt2.Rows)
                        {
                            string blockname = row["blockname"].ToString();
                            if (!firstBlocks.Contains(blockname))
                                firstBlocks.Add(blockname);
                        } //now we have the block names of the first course

                        SqlCommand cmd3 = new SqlCommand("SELECT blockname FROM academicblocks WHERE term=@term and coursename=@coursename", CommonFunctions.con);
                        cmd3.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); // şimdilik, düzelt: şu anki data böyle olduğundan term prevterm burada, normali term=currentterm olmalı
                        cmd3.Parameters.AddWithValue("@coursename", secondCourse);
                        SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                        DataTable dt3 = new DataTable();
                        da3.Fill(dt3);

                        foreach (DataRow row in dt3.Rows)
                        {
                            string blockname = row["blockname"].ToString();
                            if (!secondBlocks.Contains(blockname))
                                secondBlocks.Add(blockname);
                        } //now we have the block names of the second course. Now check whether they have a common academic block

                        bool canConflict = true;

                        List<string> intersectionBlocks = firstBlocks.Intersect(secondBlocks).ToList();
                        if (intersectionBlocks.Count > 0)
                        {
                            canConflict = false;
                        }

                        if (canConflict)
                        {
                            bool exists = false;
                            foreach (string s in coursesThatCanConflict)
                            {
                                if (s == (firstCourse + " and " + secondCourse + " can have a time conflict."))
                                {
                                    exists = true;
                                }
                            }
                            if (!exists)
                            {

                                coursesThatCanConflict.Add(firstCourse + " and " + secondCourse + " can have a time conflict.");
                            }
                        }
                    }
                }
            }

            return coursesThatCanConflict;

        }



        private List<string> findCoursesThatCanConflictforThreeCourses(string subj1, string numb1, string subj2, string numb2, string subj3, string numb3)
        {
            List<Course> setOfFirstCourses = new List<Course>();
            List<Course> setOfSecondCourses = new List<Course>();
            List<Course> setOfThirdCourses = new List<Course>();
            List<Course> allCoursesInThatTerm = new List<Course>();
            List<string> coursesThatCanConflict = new List<string>();

            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {
            }

            //Query for first set of courses, first get all courses from Timetable for that term
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT * FROM Timetable WHERE term=@term", CommonFunctions.con);
            cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            // here, get the CS 3XX courses for example
            foreach (DataRow row in dt.Rows)
            {
                Course c = new Course(row["subj"].ToString(), row["numb"].ToString(), row["section"].ToString(), row["starthour"].ToString(), row["endhour"].ToString(), row["day"].ToString(), row["building"].ToString(), row["room"].ToString());
                if (c.getSubj() == subj1 && c.getNumb().Substring(0, 1) == numb1.Substring(0, 1))
                {
                    bool exists = false;
                    foreach (Course course in setOfFirstCourses)
                    {
                        if (c.getCourseName() == course.getCourseName())
                        {
                            exists = true;
                        }
                    }
                    if (!exists)
                    {
                        setOfFirstCourses.Add(c);
                    }
                }
                if (c.getSubj() == subj2 && c.getNumb().Substring(0, 1) == numb2.Substring(0, 1))
                {
                    bool exists = false;

                    foreach (Course course in setOfSecondCourses)
                    {
                        if (c.getCourseName() == course.getCourseName())
                        {
                            exists = true;
                        }
                    }
                    if (!exists)
                    {
                        setOfSecondCourses.Add(c);
                    }
                }

                if (c.getSubj() == subj3 && c.getNumb().Substring(0, 1) == numb3.Substring(0, 1))
                {
                    bool exists = false;

                    foreach (Course course in setOfThirdCourses)
                    {
                        if (c.getCourseName() == course.getCourseName())
                        {
                            exists = true;
                        }
                    }
                    if (!exists)
                    {
                        setOfThirdCourses.Add(c);
                    }
                }

            } //now we have the first, second and third sets of courses, now for each pair check the academic blocks




            foreach (Course c1 in setOfFirstCourses)
            {
                string firstCourse = c1.getSubj() + " " + c1.getNumb();
                foreach (Course c2 in setOfSecondCourses)
                {
                    string secondCourse = c2.getSubj() + " " + c2.getNumb();
                    foreach (Course c3 in setOfThirdCourses)
                    {
                        if (!((c1 == c2) || (c1 == c3) || (c2 == c3)))
                        {
                            string thirdCourse = c3.getSubj() + " " + c3.getNumb();
                            List<string> firstBlocks = new List<string>(), secondBlocks = new List<string>(), thirdBlocks = new List<string>();

                            //get the academic blocks of c1 and c2, if they do not have any academic block in common, that means they can conflict
                            SqlCommand cmd2 = new SqlCommand("SELECT blockname FROM academicblocks WHERE term=@term and coursename=@coursename", CommonFunctions.con);
                            cmd2.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); // şimdilik, düzelt: şu anki data böyle olduğundan term prevterm burada, normali term=currentterm olmalı
                            cmd2.Parameters.AddWithValue("@coursename", firstCourse);
                            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                            DataTable dt2 = new DataTable();
                            da2.Fill(dt2);

                            foreach (DataRow row in dt2.Rows)
                            {
                                string blockname = row["blockname"].ToString();
                                if (!firstBlocks.Contains(blockname))
                                    firstBlocks.Add(blockname);
                            } //now we have the block names of the first course

                            SqlCommand cmd3 = new SqlCommand("SELECT blockname FROM academicblocks WHERE term=@term and coursename=@coursename", CommonFunctions.con);
                            cmd3.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); // şimdilik, düzelt: şu anki data böyle olduğundan term prevterm burada, normali term=currentterm olmalı
                            cmd3.Parameters.AddWithValue("@coursename", secondCourse);
                            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                            DataTable dt3 = new DataTable();
                            da3.Fill(dt3);

                            foreach (DataRow row in dt3.Rows)
                            {
                                string blockname = row["blockname"].ToString();
                                if (!secondBlocks.Contains(blockname))
                                    secondBlocks.Add(blockname);
                            }

                            SqlCommand cmd4 = new SqlCommand("SELECT blockname FROM academicblocks WHERE term=@term and coursename=@coursename", CommonFunctions.con);
                            cmd4.Parameters.AddWithValue("@term", CommonFunctions.prevTerm); // şimdilik, düzelt: şu anki data böyle olduğundan term prevterm burada, normali term=currentterm olmalı
                            cmd4.Parameters.AddWithValue("@coursename", thirdCourse);
                            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
                            DataTable dt4 = new DataTable();
                            da4.Fill(dt4);

                            foreach (DataRow row in dt4.Rows)
                            {
                                string blockname = row["blockname"].ToString();
                                if (!thirdBlocks.Contains(blockname))
                                    thirdBlocks.Add(blockname);
                            } //now we have the block names of the third course. Now check whether they have a common academic block



                            bool canConflict = true;

                            List<string> intersectionBlocks1 = firstBlocks.Intersect(secondBlocks).ToList(), intersectionBlocks2 = firstBlocks.Intersect(thirdBlocks).ToList(), interectionBlocks3 = secondBlocks.Intersect(thirdBlocks).ToList();
                            if (intersectionBlocks1.Count > 0 || intersectionBlocks2.Count > 0 || interectionBlocks3.Count > 0)
                            {
                                canConflict = false;
                            }



                            if (canConflict)
                            {
                                bool exists = false;
                                foreach (string s in coursesThatCanConflict)
                                {
                                    if (s == (firstCourse + " and " + secondCourse + " and " + thirdCourse + " can have a time conflict."))
                                    {
                                        exists = true;
                                    }
                                }
                                if (!exists)
                                {

                                    coursesThatCanConflict.Add(firstCourse + " and " + secondCourse + " and " + thirdCourse + " can have a time conflict.");
                                }
                            }
                        }
                    }

                    

                   
                }
            }

            return coursesThatCanConflict;

        }

        protected void button_submit_Click(object sender, EventArgs e)
        {
            if (dropdown_first_set_of_courses.Text != "Select One..." && dropdown_first_set_of_course_numbers.Text != "Select One..." && dropdown_second_set_of_courses.Text != "Select One..." && dropdown_second_set_of_course_numbers.Text != "Select One..." && dropdown_third_set_of_courses.Text != "Select One..." && dropdown_third_set_of_course_numbers.Text != "Select One...")
            {
                //if 3 sets of courses are selected
                string first_subj = dropdown_first_set_of_courses.Text.ToString(), second_subj = dropdown_second_set_of_courses.Text.ToString(), third_subj = dropdown_third_set_of_courses.Text.ToString();
                string first_numb = dropdown_first_set_of_course_numbers.Text.ToString(), second_numb = dropdown_second_set_of_course_numbers.Text.ToString(), third_numb = dropdown_third_set_of_course_numbers.Text.ToString();
                List<string> coursesThatCanConflict = findCoursesThatCanConflictforThreeCourses(first_subj, first_numb, second_subj, second_numb, third_subj, third_numb);
                string allCoursesThatCanCoflict = "";
                foreach (string s in coursesThatCanConflict)
                {
                    allCoursesThatCanCoflict += s + "<br/>";
                }

                label_courses_that_can_conflict.Visible = true;
                if (allCoursesThatCanCoflict == "")
                    label_courses_that_can_conflict.Text = "There are no pair courses that can have a time conflict";
                else
                    label_courses_that_can_conflict.Text = allCoursesThatCanCoflict;
            }
            else if (dropdown_first_set_of_courses.Text != "Select One..." && dropdown_first_set_of_course_numbers.Text != "Select One..." && dropdown_second_set_of_courses.Text != "Select One..." && dropdown_second_set_of_course_numbers.Text != "Select One...")
            {
                //1st and 2nd sets of courses are selected
                string first_subj = dropdown_first_set_of_courses.Text.ToString(), second_subj = dropdown_second_set_of_courses.Text.ToString();
                string first_numb = dropdown_first_set_of_course_numbers.Text.ToString(), second_numb = dropdown_second_set_of_course_numbers.Text.ToString();
                List<string> coursesThatCanConflict = findCoursesThatCanConflict(first_subj, first_numb, second_subj, second_numb);
                string allCoursesThatCanCoflict = "";
                foreach (string s in coursesThatCanConflict)
                {
                    allCoursesThatCanCoflict += s + "<br/>";
                }

                label_courses_that_can_conflict.Visible = true;
                if (allCoursesThatCanCoflict == "")
                    label_courses_that_can_conflict.Text = "There are no pair courses that can have a time conflict";
                else
                    label_courses_that_can_conflict.Text = allCoursesThatCanCoflict;

            }
            else if(dropdown_first_set_of_courses.Text != "Select One..." && dropdown_first_set_of_course_numbers.Text != "Select One..." && dropdown_third_set_of_courses.Text != "Select One..." && dropdown_third_set_of_course_numbers.Text != "Select One..." )
            {
                //1st and 3rd sets of courses are selected
                string first_subj = dropdown_first_set_of_courses.Text.ToString(), second_subj = dropdown_third_set_of_courses.Text.ToString();
                string first_numb = dropdown_first_set_of_course_numbers.Text.ToString(), second_numb = dropdown_third_set_of_course_numbers.Text.ToString();
                List<string> coursesThatCanConflict = findCoursesThatCanConflict(first_subj, first_numb, second_subj, second_numb);
                string allCoursesThatCanCoflict = "";
                foreach (string s in coursesThatCanConflict)
                {
                    allCoursesThatCanCoflict += s + "<br/>";
                }

                label_courses_that_can_conflict.Visible = true;
                if (allCoursesThatCanCoflict == "")
                    label_courses_that_can_conflict.Text = "There are no pair courses that can have a time conflict";
                else
                    label_courses_that_can_conflict.Text = allCoursesThatCanCoflict;
            }
            else if (dropdown_second_set_of_courses.Text != "Select One..." && dropdown_second_set_of_course_numbers.Text != "Select One..." && dropdown_third_set_of_courses.Text != "Select One..." && dropdown_third_set_of_course_numbers.Text != "Select One...")
            {
                //2nd and 3rd sets of courses are selected
                string first_subj = dropdown_second_set_of_courses.Text.ToString(), second_subj = dropdown_third_set_of_courses.Text.ToString();
                string first_numb = dropdown_second_set_of_course_numbers.Text.ToString(), second_numb = dropdown_third_set_of_course_numbers.Text.ToString();
                List<string> coursesThatCanConflict = findCoursesThatCanConflict(first_subj, first_numb, second_subj, second_numb);
                string allCoursesThatCanCoflict = "";
                foreach (string s in coursesThatCanConflict)
                {
                    allCoursesThatCanCoflict += s + "<br/>";
                }

                label_courses_that_can_conflict.Visible = true;
                if (allCoursesThatCanCoflict == "")
                    label_courses_that_can_conflict.Text = "There are no pair courses that can have a time conflict";
                else
                    label_courses_that_can_conflict.Text = allCoursesThatCanCoflict;
            }
            else
            {
                //error
                label_error.Visible = true;
            }
        }
    }
}
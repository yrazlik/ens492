﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENS492.Classes;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Collections;

namespace ENS492
{
    public partial class FilterByInstructor : System.Web.UI.Page
    {
        static Button clicked_button;
        static bool table_row_click = false;
        static List<string> coursesThatShouldNotConflictWithSelectedCourse;
        static List<Course> courseDetailsThatShouldNotConflictWithSelectedCourse;
        static Table visualTable;
        static string clicked_course_code;
        static string clicked_course_number;
        static string clicked_course_section;
        static string clicked_course_starthour;
        static string clicked_course_endhour;
        static string clicked_course_day;
        static int static_clicked_course_capacity_needed;
        static string selected_new_starthour, selected_new_endhour, selected_new_day;
        static List<PageControls> pageControls;
        static int COLOR_INDEX = 0;
        

        private string getPostBackControlID()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control.ID;
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            DataTable dt=new DataTable();
            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {

            }

            table_filter_instructor_schedule.EnableViewState = true;
            Console.Write("as");

            bool showScheduleButtonClicked = false;
            if (pageControls == null)
                pageControls = new List<PageControls>();


            if (!Page.IsPostBack)
            {
                SqlCommand cmd = new SqlCommand("Select subj+ ' ' + numb+ ' ' + section as fullname from Courses where term=@term order by fullname", CommonFunctions.con);
                cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                checkboxlist_courses.DataSource = dt;
                checkboxlist_courses.DataTextField = "fullname";
                checkboxlist_courses.DataBind();
                table_filter_instructor_schedule.EnableViewState = true;
                Console.Write("as");
            }
            else
            {
                foreach (string controlID in Request.Form)
                {
                    Control objControl = Page.FindControl(controlID);
                    if (objControl is Button)
                    {
                        String id = objControl.ID;
                        if (id == button_showInstructorSchedule.ID || id == button_showScheculeByCoursename.ID)
                        {
                            showScheduleButtonClicked = true;
                        }
                    }
                }

                if (showScheduleButtonClicked)
                {

                    if (pageControls != null)
                        pageControls.Clear();



                    
                }
                else
                {
                    List<string> colorIndex = new List<string>();
                    foreach (PageControls pc in pageControls)
                    {
                        if(pc.type=="button" && (!colorIndex.Contains(pc.Text)))
                            colorIndex.Add(pc.Text);
                    }

                    foreach (PageControls pc in pageControls)
                    {
                        try
                        {
                            Button b = new Button();
                            b.Text = pc.Text;
                            b.ID = pc.ID;
                            if (pc.type == "button") //demekki ders buttonu
                            {
                                b.CssClass = "roundedbuttons";
                                int ind = 0;
                                try
                                {
                                    ind = colorIndex.IndexOf(pc.Text) % 40;
                                }
                                catch (Exception exxx)
                                {
                                }
                                Color clr = CommonFunctions.colors[ind];
                                b.BackColor = clr;
                                b.Enabled = true;
                                b.EnableViewState = true;
                                b.UseSubmitBehavior = false;
                                //    b.ID = b.Text;
                                b.UseSubmitBehavior = false;
                                b.Click += new EventHandler(btn_Click);
                                b.CommandArgument = pc.xPos.ToString() + ";" + pc.yPos.ToString();
                            }
                            else //demekki link button görünümlü available-not avaliable buttonu
                            {
                                b.Click += new EventHandler(lb_Click);
                                b.UseSubmitBehavior = false;
                                b.CommandArgument = pc.argument;
                                if (b.Text == "AVAILABLE")
                                    b.CssClass = "linkbuttons";
                                else if (b.Text == "NOT AVAILABLE")
                                    b.CssClass = "linkbuttonsred";
                                else if (b.Text == "NO ROOM")
                                    b.CssClass = "linkbuttonsyellow";
                            }


                            table_filter_instructor_schedule.Rows[pc.xPos].Cells[pc.yPos].Controls.Add(b);
                        }
                        catch (Exception excep)
                        {

                        }
                    }
                }
            }
        }

        public string getDayByIndex(int index)
        {

            if (index == 1)
                return "M";
            else if (index == 2)
                return "T";
            else if (index == 3)
                return "W";
            else if (index == 4)
                return "R";
            else if (index == 5)
                return "F";
            else if (index == 6)
                return "S";

            return "NULL";
        }

        public int getDayIndex(String day)
        {
            if (day == "M")
                return 1;
            else if (day == "T")
                return 2;
            else if (day == "W")
                return 3;
            else if (day == "R")
                return 4;
            else if (day == "F")
                return 5;
            else if (day == "S")
                return 6;

            return -1;
        }

        public void clearTable()
        {
            for (int i = 1; i < 14; i++)
            {
                for (int j = 1; j < 7; j++)
                {
                    table_filter_instructor_schedule.Rows[i].Cells[j].Text = "";
                    table_filter_instructor_schedule.Rows[i].Cells[j].BackColor = Color.Empty;
                    visualTable = table_filter_instructor_schedule;
                }
            }
        }

        public int getHourByIndex(int index)
        {
            if (index == 1)
                return 8;
            else if (index == 2)
                return 9;
            else if (index == 3)
                return 10;
            else if (index == 4)
                return 11;
            else if (index == 5)
                return 12;
            else if (index == 6)
                return 13;
            else if (index == 7)
                return 14;
            else if (index == 8)
                return 15;
            else if (index == 9)
                return 16;
            else if (index == 10)
                return 17;
            else if (index == 11)
                return 18;
            else if (index == 12)
                return 19;
            else if (index == 13)
                return 20;

            return -1;
        }

        public int getHourIndex(int hour)
        {
            if (hour == 8)
                return 1;
            else if (hour == 9)
                return 2;
            else if (hour == 10)
                return 3;
            else if (hour == 11)
                return 4;
            else if (hour == 12)
                return 5;
            else if (hour == 13)
                return 6;
            else if (hour == 14)
                return 7;
            else if (hour == 15)
                return 8;
            else if (hour == 16)
                return 9;
            else if (hour == 17)
                return 10;
            else if (hour == 18)
                return 11;
            else if (hour == 19)
                return 12;
            else if (hour == 20)
                return 13;

            return -1;
        }

        public bool courseExists(String courseName, ArrayList courses)
        {
            foreach (Course c in courses)
            {
                if (c.getCourseName() == courseName)
                    return true;
            }

            return false;
        }

        public Color getCourseColor(String courseName, ArrayList courses)
        {
            foreach (Course c in courses)
            {
                if (c.getCourseName() == courseName)
                    return c.getColor();
            }

            return Color.Black;
        }


        public int getCourseColorFromIndex(string courseName, ArrayList courses)
        {
            int ind = 0;
            foreach(Course c in courses)
            {
                if(c.getCourseName() == courseName)
                {
                    return (ind % 40);
                }
                else
                {
                    ind++;
                }
            }
            return ind;
        }

        protected void button_showInstructorSchedule_Click(object sender, EventArgs e)
        {
            clicked_button = null;
            table_filter_instructor_schedule.EnableViewState = true;
            int colorIndex = 0;
            string instructor = dropdown_all_instructors.Text;
            ArrayList courses = new ArrayList();

            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {

            }
            SqlCommand cmd;

            // seçilen instructorun derslerini al. (Term ??????)
            cmd = new SqlCommand("SELECT [crn], [subj], [numb], [section] FROM Courses WHERE instructor = @instructor AND term=@term", CommonFunctions.con);
            cmd.Parameters.AddWithValue("@instructor", instructor);
            cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            clearTable();
            foreach (DataRow dr in dt.Rows)  //o instructurun her bir dersi için dersin saatlerini gününü vs. bul ve tabloya ekle
            {
                string subj = dr["subj"].ToString(), numb = dr["numb"].ToString(), section = dr["section"].ToString(), crn = dr["crn"].ToString();


                if (!courseExists(subj + numb + section, courses))
                {
                    Course course = new Course(subj, numb, section, crn, CommonFunctions.colors[colorIndex]); colorIndex++;
                    courses.Add(course);

                }  //courses arrayi o instructurun tüm courselarını tutuyor.

                //her ders için teker teker o dersin ders programını al
                SqlCommand com = new SqlCommand("SELECT [starthour], [endhour], [day], [building], [room] FROM TimeTable WHERE crn=@crn AND term=@term", CommonFunctions.con);
                com.Parameters.AddWithValue("@crn", crn);
                com.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter adptr = new SqlDataAdapter(com);
                DataTable table = new DataTable();
                adptr.Fill(table);

                foreach (DataRow row in table.Rows)
                {
                    try
                    {
                        string starthour = row["starthour"].ToString(), endhour = row["endhour"].ToString(), day = row["day"].ToString(), building = row["building"].ToString(), room = row["room"].ToString();

                        int start = Convert.ToInt32(starthour.Substring(0, starthour.Length - 2));
                        int end = Convert.ToInt32(endhour.Substring(0, endhour.Length - 2));

                        int startindex = getHourIndex(start);
                        int endindex = getHourIndex(end);

                        int dayindex = getDayIndex(day);

                        for (int i = startindex; i < endindex; i++)  //tabloya ekle
                        {


                            Button b = new Button();
                            b.Text = subj + " " + numb + " " + section;
                            //   b.Attributes.Add("onclick", "popWin()");
                            b.CssClass = "roundedbuttons";

                            Color clr = CommonFunctions.colors[getCourseColorFromIndex(subj + numb + section, courses)];
                            
                            b.BackColor = clr;
                            b.Enabled = true;
                            b.EnableViewState = true;
                            b.UseSubmitBehavior = false;
                            //    b.ID = b.Text;
                            b.UseSubmitBehavior = false;
                            b.Click += new EventHandler(btn_Click);

                            string cellPosition = (i).ToString() + ";" + (dayindex).ToString();
                            b.CommandArgument = cellPosition;
                            table_row_click = true;
                            table_filter_instructor_schedule.Rows[i].Cells[dayindex].Controls.Add(b);
                            
                            pageControls.Add(new PageControls(i, dayindex, null, subj + " " + numb + " " + section, "button"));
                            //   ph.Controls.Add(b);
                            visualTable = table_filter_instructor_schedule;

                        }
                    }
                    catch (Exception exx)
                    {
                        Console.WriteLine(exx.Message);
                    }
                }
            }

            Console.Write("");
            CommonFunctions.con.Close();
        }


        public bool checkConflict(int i1, int j1, int i2, int j2) //returns true if there is a conflict
        {
            if (i1 == i2)
                return true;
            else if (j1 == j2)
                return true;
            
            return false;
        }



        protected void btn_Click(object sender, EventArgs e)
        {
            foreach (PageControls pc in pageControls.ToList())
            {
                if (pc.type == "linkbutton")
                    pageControls.Remove(pc);
            }
            Button b = (Button)sender;
            //  btnPopUp_ModalPopupExtender.TargetControlID = b.ID;
            //  b.Controls.Add(btnPopUp_ModalPopupExtender);
            //    btnPopUp_ModalPopupExtender.Show();
            //   TextBox1.Text = b.Text;

            string selectedCourse = b.Text.ToString();

            var split = selectedCourse.Split(' ');
            string selectedCourseName = split[0] + " " + split[1];
            string selectedCourseSection = split[2];
            clicked_course_code = split[0];
            clicked_course_number = split[1];
            clicked_course_section = split[2];


            string cellPositions = b.CommandArgument;
            var values = cellPositions.Split(';');
            int ii = Convert.ToInt32(values[0].ToString()), jj = Convert.ToInt32(values[1].ToString());//tıklanan yerin tablodaki koordinatları

            clicked_course_day = getDayByIndex(jj);
            int startt = getHourByIndex(ii);
            clicked_course_starthour = startt.ToString() + "40";
            clicked_course_endhour = (startt + 1).ToString() + "30"; 
            int clicked_course_capacity_needed=300;
           

            try
            {
                CommonFunctions.con.Open();
            }
            catch (Exception ex)
            {
            }

            //get the capacity of selected course
            SqlCommand sqlCommand = new SqlCommand("SELECT capacity FROM Courses WHERE term=@term AND subj=@subj AND numb=@numb AND section=@section", CommonFunctions.con);
            sqlCommand.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            sqlCommand.Parameters.AddWithValue("@subj", clicked_course_code);
            sqlCommand.Parameters.AddWithValue("@numb", clicked_course_number);
            sqlCommand.Parameters.AddWithValue("@section",clicked_course_section);
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            DataTable datatable = new DataTable();
            adapter.Fill(datatable);
            foreach(DataRow row in datatable.Rows)
            {
                clicked_course_capacity_needed = Convert.ToInt32(row["capacity"].ToString());
                static_clicked_course_capacity_needed = clicked_course_capacity_needed;
                break;
            }
            



            //tıklanan derse ait tüm academic blockları al
            SqlCommand cmd = new SqlCommand("SELECT blockname from academicblocks where term=@term AND coursename=@coursename", CommonFunctions.con);
            cmd.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
            cmd.Parameters.AddWithValue("@coursename", selectedCourseName);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            List<string> blocksThatContainsSelectedCourse = new List<string>();

            foreach (DataRow row in dt.Rows)
            {
                blocksThatContainsSelectedCourse.Add(row["blockname"].ToString());
            } //now we get the block names that contains the clicked course, now get all the courses in that blocks

            coursesThatShouldNotConflictWithSelectedCourse = new List<string>();


            //bu academic blocklardaki tüm dersleri al, çünkü tıklanan ders bu blocklardaki hiçbir dersle çakışmamalı
            foreach (string blockname in blocksThatContainsSelectedCourse)
            {
                SqlCommand com = new SqlCommand("SELECT coursename, coursesection FROM academicblocks WHERE term=@term AND blockname=@blockname", CommonFunctions.con);
                com.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);
                com.Parameters.AddWithValue("@blockname", blockname);
                SqlDataAdapter da2 = new SqlDataAdapter(com);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                foreach (DataRow row in dt2.Rows)
                {
                    if (!coursesThatShouldNotConflictWithSelectedCourse.Contains(row["coursename"].ToString() + " " + row["coursesection"].ToString()))
                        coursesThatShouldNotConflictWithSelectedCourse.Add(row["coursename"].ToString() + " " + row["coursesection"].ToString());
                }

            }//now we have all course names that should not conflict with the clicked course, now get the details about that course

            courseDetailsThatShouldNotConflictWithSelectedCourse = new List<Course>();

            //çakışmaması gereken derslerin tüm detaylarını al, ders saatleri ders günü sınıf vb.
            foreach (string course in coursesThatShouldNotConflictWithSelectedCourse)
            {
                var splitCourse = course.Split(' ');
                string subj = splitCourse[0], numb = splitCourse[1], section = splitCourse[2];

                
                SqlCommand commnd = new SqlCommand("SELECT starthour,endhour,day,building,room FROM Timetable WHERE term=@term AND subj=@subj AND numb=@numb AND section=@section", CommonFunctions.con);
                commnd.Parameters.AddWithValue("@subj", subj);
                commnd.Parameters.AddWithValue("@numb", numb);
                commnd.Parameters.AddWithValue("@section", section);
                commnd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter da3 = new SqlDataAdapter(commnd);
                DataTable dt3 = new DataTable();
                da3.Fill(dt3);


                foreach (DataRow row in dt3.Rows)
                {
                    Course newCourse = new Course(subj, numb, section, row["starthour"].ToString(), row["endhour"].ToString(), row["day"].ToString(), row["building"].ToString(), row["room"].ToString());
                    courseDetailsThatShouldNotConflictWithSelectedCourse.Add(newCourse);
                }
            }//now we have all the details about these courses, it's time to color and fill the visual table

            for (int i = 1; i <= 13; i++)
            {
                for (int j = 1; j <= 6; j++)
                {
                    foreach (Control control in table_filter_instructor_schedule.Rows[i].Cells[j].Controls)
                    {
                        Button but = (Button)control;

                        if (but != null && (but.Text == "AVAILABLE" || but.Text == "NOT AVAILABLE"))
                            table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Remove(but);
                    }
                }
            }

            for (int i = 1; i <= 13; i++)
            {
                for (int j = 1; j <= 6; j++)
                {


                    bool conflict = false;

                    foreach (Course c in courseDetailsThatShouldNotConflictWithSelectedCourse)
                    {
                        string starthour = c.getStartHour(), endhour = c.getEndHour(), day = c.getDay(), building = c.getBuilding(), room = c.getRoom();
                        try
                        {
                            int start = Convert.ToInt32(starthour.Substring(0, starthour.Length - 2));
                            int end = Convert.ToInt32(endhour.Substring(0, endhour.Length - 2));


                            int startindex = getHourIndex(start);
                            int endindex = getHourIndex(end);

                            int dayindex = getDayIndex(day);

                            if (dayindex == j && (checkConflict(i, i + 1, startindex, endindex)))
                            {
                                conflict = true;
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                        }


                    }
                    if (!conflict)
                    {
                        if (table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Count == 0)
                        {
                            //check whether room capacity is enough
                            List<Room> nonConflictingRooms = getNonConflictingRooms(i, j);
                            bool enougCapacityFound = false;
                            foreach (Room room in nonConflictingRooms)
                            {
                                if (room.getCapacity() >= clicked_course_capacity_needed)
                                {
                                    enougCapacityFound = true;

                                }
                            }

                            if (enougCapacityFound)
                            {

                                Button lb = new Button();
                                lb.Text = "AVAILABLE";
                                //   b.Attributes.Add("onclick", "popWin()");
                                string cellPosition = i.ToString() + ";" + j.ToString();
                                lb.ID = cellPosition;
                                //  lb.Attributes.Add("onClick", "return false;");
                                lb.Click += new EventHandler(lb_Click);
                                lb.UseSubmitBehavior = false;
                                lb.CssClass = "linkbuttons";
                                // clicked_button = lb;
                                table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Add(lb);
                                try
                                {
                                    pageControls.Add(new PageControls(i, j, null, "AVAILABLE", "linkbutton", cellPosition));
                                }
                                catch (Exception exx)
                                {

                                }
                                visualTable = table_filter_instructor_schedule;
                            }
                            else
                            {
                                Button lb = new Button();
                                lb.Text = "NO ROOM";
                                //   b.Attributes.Add("onclick", "popWin()");
                                string cellPosition = i.ToString() + ";" + j.ToString();
                                lb.ID = cellPosition;
                                //  lb.Attributes.Add("onClick", "return false;");
                                lb.Click += new EventHandler(lb_Click);
                                lb.UseSubmitBehavior = false;
                                lb.CssClass = "linkbuttonsyellow";
                                // clicked_button = lb;
                                table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Add(lb);
                                try
                                {
                                    pageControls.Add(new PageControls(i, j, null, "NO ROOM", "linkbutton", cellPosition));
                                }
                                catch (Exception exx)
                                {

                                }
                                visualTable = table_filter_instructor_schedule;
                            }
                        }
                    }
                    else
                    {
                        if (table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Count == 0)
                        {
                            Button lb = new Button();
                            lb.Text = "NOT AVAILABLE";
                            lb.CssClass = "linkbuttonsred";

                            //   b.Attributes.Add("onclick", "popWin()");
                            string cellPosition = i.ToString() + ";" + j.ToString();
                            lb.CommandArgument = cellPosition;
                            /*  Control control = new Control();
                              control.ID = cellPosition;
                              lb.Controls.Add(control);*/
                            lb.ID = cellPosition;
                            lb.UseSubmitBehavior = false;

                            lb.ForeColor = Color.OrangeRed;
                            //   clicked_button = lb;
                            pageControls.Add(new PageControls(i, j, null, "NOT AVAILABLE", "linkbutton", cellPosition));
                            try
                            {
                                table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Add(lb);
                            }
                            catch (Exception exx)
                            {

                            }
                            visualTable = table_filter_instructor_schedule;
                        }
                    }

                }

            }


            /*  try
              {
                  Button b = (Button)sender;
                  string courseName = b.Text;

                  for (int i = 1; i < 14; i++)
                  {
                      for (int j = 1; j < 7; j++)
                      {
                          if (table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Count == 0)
                          {
                              LinkButton lb = new LinkButton();
                              lb.Text = "AVAILABLE";
                              lb.ForeColor = Color.LightGreen;
                              table_filter_instructor_schedule.Rows[i].Cells[j].Controls.Add(lb);
                          }

                      }

                  }









                  Console.Write("");
              }
              catch (Exception)
              {

              }*/
        }


        List<Room> getNonConflictingRooms(int i, int j)
        {
            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
            }
            catch (Exception exc)
            {
            }

            SqlCommand cm = new SqlCommand("SELECT day,starthour,endhour,building,room FROM Timetable WHERE term=@term", CommonFunctions.con);//get all the courses
            cm.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            //tüm dersleri alıp, herhangi bir ders var mı o saatte bakacağız, o saatte ders olan yerlerin odalarını available roomlardan cıkaracağız

            SqlDataAdapter daa = new SqlDataAdapter(cm);
            DataTable dtt = new DataTable();
            daa.Fill(dtt); //tüm dersleri aldık

            List<Course> allCourses = new List<Course>();
            List<string> conflictingRooms = new List<string>();
            foreach (DataRow row in dtt.Rows)
            {
                Course newCourse = new Course(row["day"].ToString(), row["starthour"].ToString(), row["endhour"].ToString(), row["building"].ToString(), row["room"].ToString());
                allCourses.Add(newCourse);
            }

            int indexx = 0;
            foreach (Course c in allCourses)
            {
                int startt = Convert.ToInt32(c.getStartHour().Substring(0, c.getStartHour().Length - 2));
                int endd = Convert.ToInt32(c.getEndHour().Substring(0, c.getEndHour().Length - 2));

                int startindex = getHourIndex(startt);
                int endindex = getHourIndex(endd);
                indexx++;
                if (getDayIndex(c.getDay()) == j && (checkConflict(i, i + 1, startindex, endindex)))
                {
                    conflictingRooms.Add(c.getBuilding() + ";" + c.getRoom());
                }
            }//now we have all conflicting rooms, now go to allcourses table and get rooms that are not conflicting


            dropdown_available_rooms.Visible = true;
            ModalOKButton.Enabled = true;
            SqlCommand sqcm = new SqlCommand("SELECT building,room FROM allrooms", CommonFunctions.con);
            SqlDataAdapter dap = new SqlDataAdapter(sqcm);
            DataTable dat = new DataTable();
            dap.Fill(dat);
            List<string> allRooms = new List<string>(), nonConflictingRooms = new List<string>();
            foreach (DataRow row in dat.Rows)
            {
                allRooms.Add(row["building"].ToString() + ";" + row["room"].ToString());
            }//get all the rooms

            //şimdi de çakışma olan odaları tüm odalardan çıkar
            foreach (string s in allRooms)
            {
                if (!conflictingRooms.Contains(s))
                {
                    nonConflictingRooms.Add(s);
                }
            } //now nonConflictingRooms contains all available rooms

            List<Room> nonConflictingRoomsList = new List<Room>();
            foreach (string room in nonConflictingRooms)
            {
                var values = room.Split(';');
                string bl = values[0], rm=values[1];
                SqlCommand sqlc = new SqlCommand("SELECT capacity FROM allrooms WHERE building=@building AND  room=@room", CommonFunctions.con);
                sqlc.Parameters.AddWithValue("@building", bl);
                sqlc.Parameters.AddWithValue("@room", rm);
                SqlDataAdapter a = new SqlDataAdapter(sqlc);
                DataTable t = new DataTable();
                a.Fill(t);

                foreach (DataRow r in t.Rows)
                {
                    string cap = r["capacity"].ToString();
                    int capacity = Convert.ToInt32(cap);
                    Room newRoom = new Room(bl, rm, capacity);
                    nonConflictingRoomsList.Add(newRoom);   //now nonConflictingRoomsList contains all available rooms as a Room class, not just string
                }

            }


            return nonConflictingRoomsList;
        }

        void lb_Click(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;

                string clickedDay,clickedStartHour,clickedEndHour,clickedCourseCapacity;

                string cellPositions = b.CommandArgument;
                var values = cellPositions.Split(';');
                int i = Convert.ToInt32(values[0].ToString()), j = Convert.ToInt32(values[1].ToString());//tıklanan yerin tablodaki koordinatları

                clickedDay = getDayByIndex(j);
                int start = getHourByIndex(i);
                clickedStartHour = start.ToString() + "40";
                clickedEndHour = (start + 1).ToString() + "30"; //tıklanan link buttonunun start ve end hourlar elimizde, şimdi timetabledan bu saatlerde available olmayan odaları bul
                selected_new_starthour = clickedStartHour;
                selected_new_endhour = clickedEndHour;
                selected_new_day = clickedDay;
                try
                {
                    CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                    CommonFunctions.con.Open();
                }
                catch (Exception exc)
                {
                }

                SqlCommand cm = new SqlCommand("SELECT day,starthour,endhour,building,room FROM Timetable WHERE term=@term", CommonFunctions.con);//get all the courses
                //tüm dersleri alıp, herhangi bir ders var mı o saatte bakacağız, o saatte ders olan yerlerin odalarını available roomlardan cıkaracağız
                cm.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter daa = new SqlDataAdapter(cm);
                DataTable dtt = new DataTable();
                daa.Fill(dtt); //tüm dersleri aldık

                List<Course> allCourses = new List<Course>();
                List<string> conflictingRooms = new List<string>();
                foreach (DataRow row in dtt.Rows)
                {
                    Course newCourse = new Course(row["day"].ToString(), row["starthour"].ToString(), row["endhour"].ToString(), row["building"].ToString(), row["room"].ToString());
                    allCourses.Add(newCourse);
                }

                int indexx = 0;
                foreach (Course c in allCourses)
                {
                    int startt = Convert.ToInt32(c.getStartHour().Substring(0, c.getStartHour().Length - 2));
                    int endd = Convert.ToInt32(c.getEndHour().Substring(0, c.getEndHour().Length - 2));

                    int startindex = getHourIndex(startt);
                    int endindex = getHourIndex(endd);

                    indexx++;
                    if (getDayIndex(c.getDay()) == j && (checkConflict(i, i + 1, startindex, endindex)))
                    {
                        conflictingRooms.Add(c.getBuilding() + ";" + c.getRoom());
                    }
                }//now we have all conflicting rooms, now go to allcourses table and get rooms that are not conflicting


                if (b.Text == "AVAILABLE")
                {
                    dropdown_available_rooms.Visible=true;
                    ModalOKButton.Enabled = true;
                    SqlCommand sqcm = new SqlCommand("SELECT building,room FROM allrooms", CommonFunctions.con);
                    SqlDataAdapter dap = new SqlDataAdapter(sqcm);
                    DataTable dat = new DataTable();
                    dap.Fill(dat);
                    List<string> allRooms = new List<string>(), nonConflictingRooms = new List<string>();
                    foreach (DataRow row in dat.Rows)
                    {
                        allRooms.Add(row["building"].ToString() + ";" + row["room"].ToString());
                    }//get all the rooms

                    //şimdi de çakışma olan odaları tüm odalardan çıkar
                    foreach (string s in allRooms)
                    {
                        if (!conflictingRooms.Contains(s))
                        {
                            nonConflictingRooms.Add(s);
                        }
                    } //now allRooms contains all available rooms

                    List<Room> nonConflictingRoomsList = new List<Room>();
                    foreach (string room in nonConflictingRooms)
                    {
                        var val = room.Split(';');
                        string bl = val[0], rm = val[1];
                        SqlCommand sqlc = new SqlCommand("SELECT capacity FROM allrooms WHERE building=@building AND  room=@room", CommonFunctions.con);
                        sqlc.Parameters.AddWithValue("@building", bl);
                        sqlc.Parameters.AddWithValue("@room", rm);
                        SqlDataAdapter a = new SqlDataAdapter(sqlc);
                        DataTable t = new DataTable();
                        a.Fill(t);

                        foreach (DataRow r in t.Rows)
                        {
                            string cap = r["capacity"].ToString();
                            int capacity = Convert.ToInt32(cap);
                            Room newRoom = new Room(bl, rm, capacity);
                            nonConflictingRoomsList.Add(newRoom);   //now nonConflictingRoomsList contains all available rooms as a Room class, not just string
                        }

                    }
                    nonConflictingRooms.Clear();
                    foreach (Room r in nonConflictingRoomsList)
                    {
                        if (r.getCapacity() >= static_clicked_course_capacity_needed)
                        {
                            if (!nonConflictingRooms.Contains(r.getBuilding() + ";" + r.getRoom()))
                            {
                                nonConflictingRooms.Add(r.getBuilding() + ";" + r.getRoom());
                            }
                        }
                    }
                                       


                    //term simdilik boyle
                    SqlCommand commnd = new SqlCommand("SELECT starthour,endhour,day,building,room FROM Timetable WHERE term=@term AND subj=@subj AND numb=@numb AND section=@section", CommonFunctions.con);
                    commnd.Parameters.AddWithValue("@subj", clicked_course_code);
                    commnd.Parameters.AddWithValue("@numb", clicked_course_number);
                    commnd.Parameters.AddWithValue("@section", clicked_course_section);
                    commnd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    SqlDataAdapter da3 = new SqlDataAdapter(commnd);
                    DataTable dt3 = new DataTable();
                    da3.Fill(dt3);

                    List<Course> currentSchedule = new List<Course>();
                    foreach (DataRow row in dt3.Rows)
                    {
                        Course newCourse = new Course(clicked_course_code, clicked_course_number, clicked_course_section, row["starthour"].ToString(), row["endhour"].ToString(), row["day"].ToString(), row["building"].ToString(), row["room"].ToString());
                        currentSchedule.Add(newCourse);
                    }
                    string schedule="";
                    foreach(Course c in currentSchedule)
                    {
                        schedule+=c.getDay()+": " + c.getStartHour()+"-"+c.getEndHour()+" "+c.getBuilding()+" "+c.getRoom()+", "; //current schedule of the clicked course



                    }

                    schedule += "if you want to change that lecture hours to the selected day and hour please choose one of the available rooms from the list:";

                    label_current_schedule.Text = "The current schedule of the course " + clicked_course_code + clicked_course_number + " " + clicked_course_section + " is as follows: " + schedule;
                    dropdown_available_rooms.Items.Clear();
                    foreach (string s in nonConflictingRooms) //conflict olmayan odaları ekle
                    {
                        var v = s.Split(';');
                        try
                        {
                            string bldg = v[0], rm = v[1];
                            dropdown_available_rooms.Items.Add(new ListItem(bldg + " " + rm));
                        }
                        catch (Exception exx)
                        {
                        }
                    }
                    
                    dropdown_available_rooms.Visible = true;

                }
                else if (b.Text == "NOT AVAILABLE")
                {
                    dropdown_available_rooms.Visible = false;
                    ModalOKButton.Enabled = false;//burasiiiiiiii
                    SqlCommand cmd = new SqlCommand("SELECT DISTINCT blockname from academicblocks where term=@term AND coursename=@coursename", CommonFunctions.con);
                 
                    cmd.Parameters.AddWithValue("@coursename", clicked_course_code + " " + clicked_course_number);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    List<string> blocksThatContainsSelectedCourse = new List<string>();

                    foreach (DataRow row in dt.Rows)
                    {
                        blocksThatContainsSelectedCourse.Add(row["blockname"].ToString());
                    } //now we get the block names that contains the clicked course, now get all the courses in that blocks

                   
                    List<String> coursesThatCannotConflictWithSelectedCourse = new List<string>();
                    foreach (string blockname in blocksThatContainsSelectedCourse)
                    {
                        SqlCommand com = new SqlCommand("SELECT coursename, coursesection FROM academicblocks WHERE term=@term AND blockname=@blockname", CommonFunctions.con);
                        com.Parameters.AddWithValue("@term", CommonFunctions.prevTerm);//şimdilik
                        com.Parameters.AddWithValue("@blockname", blockname);
                        SqlDataAdapter da2 = new SqlDataAdapter(com);
                        DataTable dt2 = new DataTable();
                        da2.Fill(dt2);

                        foreach (DataRow row in dt2.Rows)
                        {
                            if (!coursesThatCannotConflictWithSelectedCourse.Contains(row["coursename"].ToString() + " " + row["coursesection"].ToString()))
                                coursesThatCannotConflictWithSelectedCourse.Add(row["coursename"].ToString() + " " + row["coursesection"].ToString());
                        }

                    }

                    List<Course> courseDetailsThatCanNotConflictWithSelectedCourse = new List<Course>();
                    foreach (string course in coursesThatShouldNotConflictWithSelectedCourse)
                    {
                        var splitCourse = course.Split(' ');
                        string subj = splitCourse[0], numb = splitCourse[1], section = splitCourse[2];

                        //TERM'i düzelt sonra yine burada!!!
                        SqlCommand commnd = new SqlCommand("SELECT starthour,endhour,day,building,room FROM Timetable WHERE term=@term AND subj=@subj AND numb=@numb AND section=@section", CommonFunctions.con);
                        commnd.Parameters.AddWithValue("@subj", subj);
                        commnd.Parameters.AddWithValue("@numb", numb);
                        commnd.Parameters.AddWithValue("@section", section);
                        commnd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                        SqlDataAdapter da3 = new SqlDataAdapter(commnd);
                        DataTable dt3 = new DataTable();
                        da3.Fill(dt3);


                        foreach (DataRow row in dt3.Rows)
                        {
                            Course newCourse = new Course(subj, numb, section, row["starthour"].ToString(), row["endhour"].ToString(), row["day"].ToString(), row["building"].ToString(), row["room"].ToString());
                            courseDetailsThatCanNotConflictWithSelectedCourse.Add(newCourse);
                        }
                    }//now we have all the details about these courses

                    List<Course> coursesThatAreInTheSameHour = new List<Course>();
                    foreach (Course c in courseDetailsThatCanNotConflictWithSelectedCourse)
                    {
                        int startt = Convert.ToInt32(c.getStartHour().Substring(0, c.getStartHour().Length - 2));
                        int endd = Convert.ToInt32(c.getEndHour().Substring(0, c.getEndHour().Length - 2));

                        int startindex = getHourIndex(startt);
                        int endindex = getHourIndex(endd);

                        if (getDayIndex(c.getDay()) == j && (checkConflict(i, i + 1, startindex, endindex)))
                        {
                            coursesThatAreInTheSameHour.Add(c);
                        }
                    }

                    string conflictingCourses = "";

                    foreach (Course co in coursesThatAreInTheSameHour)
                    {
                        conflictingCourses += co.getCourseName()+",";
                    }

                    label_current_schedule.Text = "You cannot move the selected course here because the following courses are in the same academic block and conflicts with the selected hour: "+ conflictingCourses;
                    dropdown_available_rooms.Visible = false;
                    List<String> blocksToVisualize = new List<string>();
                    foreach (String s in blocksThatContainsSelectedCourse)
                    {
                        if (!blocksToVisualize.Contains(s))
                            blocksToVisualize.Add(s);
                    }

                    visualizeBlocks(blocksToVisualize);

                }
                else if(b.Text == "NO ROOM")
                {
                    dropdown_available_rooms.Visible = false;
                    label_current_schedule.Text = "There are no available rooms";
                    ModalOKButton.Enabled = false;
                }


                btnPopUp_ModalPopupExtender.Show();


            }
            catch (Exception ex)
            {
                clicked_button = null;
            }
        }

        public void visualizeBlocks(List<String> intersectedBlocks)
        {
            Table tableVisual = table_visualize_blocks;
            //  tableVisual.Rows.Clear();
            try
            {
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();

            }
            catch (Exception ex)
            {
            }
            int index = 0;

            foreach (String blockName in intersectedBlocks)
            {
                try
                {
                    DataTable subjects = new DataTable();

                    DataTable dt = new DataTable();

                    SqlCommand cmd = new SqlCommand("SELECT [coursename], [coursesection] FROM academicblocks where blockname=@blockname AND term=@term", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@blockname", blockName);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    Table t = new Table();
                    t.CssClass = "singletable";
                    //    t.Attributes.Add("CssClass", "singletable");
                    t.BorderWidth = 1;
                    //    t.GridLines = GridLines.Horizontal;

                    TableRow row = new TableRow();
                    TableCell cl = new TableCell();
                    //     cl.BackColor=Color.FromArgb(240,232,146);
                    cl.Text = blockName;
                    row.Cells.Add(cl);
                    t.Rows.Add(row);




                    foreach (DataRow r in dt.Rows)
                    {
                        TableRow tr = new TableRow();
                        TableCell c = new TableCell();
                        c.Text = r["coursename"] + " " + r["coursesection"];
                        //      c.BackColor = Color.FromArgb(161, 240, 204);
                        tr.Cells.Add(c);
                        t.Rows.Add(tr);
                    }

                    tableVisual.Rows[0].Cells[index].Controls.Add(t);

                }
                catch (Exception e)
                {
                    CommonFunctions.con.Close();
                }
                index++;
                CommonFunctions.con.Close();

            }

        }

        protected void ModalOKButton_Click(object sender, EventArgs e)
        {
            string selectedRooom = dropdown_available_rooms.SelectedValue.ToString();
            var values = selectedRooom.Split(' ');
            string newbldg = values[0], newroom = values[1];
            btnPopUp_ModalPopupExtender.Hide();
            //Update the database, and put the course to its new hours

            //term şimdilik
            SqlCommand commnd = new SqlCommand("UPDATE Timetable SET starthour=@newstarthour, endhour=@newendhour, day=@newday, building=@newbuilding, room=@newroom WHERE subj=@subj AND numb=@numb AND section=@section AND term=@term AND starthour=@starthour AND endhour=@endhour AND day=@selectedday", CommonFunctions.con);
            commnd.Parameters.AddWithValue("@newstarthour", selected_new_starthour);
            commnd.Parameters.AddWithValue("@newendhour", selected_new_endhour);
            commnd.Parameters.AddWithValue("@newday", selected_new_day);
            commnd.Parameters.AddWithValue("@newbuilding", newbldg);
            commnd.Parameters.AddWithValue("@newroom", newroom);
            commnd.Parameters.AddWithValue("@subj", clicked_course_code);
            commnd.Parameters.AddWithValue("@numb", clicked_course_number);
            commnd.Parameters.AddWithValue("@section", clicked_course_section);
            commnd.Parameters.AddWithValue("@starthour", clicked_course_starthour);
            commnd.Parameters.AddWithValue("@endhour", clicked_course_endhour);
            commnd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
            commnd.Parameters.AddWithValue("@selectedday", clicked_course_day);
            commnd.ExecuteNonQuery();
       //     pageControls.Remove(new PageControls(Convert.ToInt32(clicked_course_starthour.Substring(0, clicked_course_starthour.Length - 2)),getDayIndex(clicked_course_day), null, clicked_course_code + " " + clicked_course_number + " " + clicked_course_section, "button"));
       //     pageControls.Add(new PageControls(Convert.ToInt32(selected_new_starthour.Substring(0, selected_new_starthour.Length - 2)), getDayIndex(selected_new_day), null, clicked_course_code + " " + clicked_course_number + " " + clicked_course_section, "button"));
            Response.Redirect(Request.RawUrl);
       
            
            /*foreach (PageControls pc in pageControls)
            {
                if (pc.type == "button")
                {
                    if ((pc.Text == clicked_course_code + " " + clicked_course_number + " " + clicked_course_section) && (pc.xPos == getHourIndex(Convert.ToInt32(clicked_course_starthour.Substring(0,clicked_course_starthour.Length - 2)))) && pc.yPos == getDayIndex(clicked_course_day))
                    {
                        pc.xPos = getHourIndex(Convert.ToInt32(selected_new_starthour.Substring(0, selected_new_starthour.Length - 2)));
                        pc.yPos = getDayIndex(selected_new_day);
                    }
                }
            }
            Page_Load(this, EventArgs.Empty);
              
              */
         //   Page_Load(null, EventArgs.Empty);
        }

        protected void btnPopUp_Click(object sender, EventArgs e)
        {

        }




        protected void button_showScheculeByCoursename_Click(object sender, EventArgs e)
        {
            List<string> selectedCourses = new List<string>();

            foreach (ListItem item in checkboxlist_courses.Items)
            {
                if (item.Selected)
                    selectedCourses.Add(item.Text.ToString());
            }

            foreach (string course in selectedCourses)
            {
                var values = course.Split(' ');
                string subj = values[0], numb = values[1], section = values[2];
                SqlCommand com = new SqlCommand("SELECT [starthour], [endhour], [day], [building], [room] FROM TimeTable WHERE subj=@subj AND numb=@numb AND section=@section AND term=@term", CommonFunctions.con);
                com.Parameters.AddWithValue("@subj", subj);
                com.Parameters.AddWithValue("@numb", numb);
                com.Parameters.AddWithValue("@section", section);
                com.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                SqlDataAdapter adptr = new SqlDataAdapter(com);
                DataTable table = new DataTable();
                adptr.Fill(table);

                foreach (DataRow row in table.Rows)
                {
                    try
                    {
                        string starthour = row["starthour"].ToString(), endhour = row["endhour"].ToString(), day = row["day"].ToString(), building = row["building"].ToString(), room = row["room"].ToString();

                        int start = Convert.ToInt32(starthour.Substring(0, starthour.Length - 2));
                        int end = Convert.ToInt32(endhour.Substring(0, endhour.Length - 2));

                        int startindex = getHourIndex(start);
                        int endindex = getHourIndex(end);

                        int dayindex = getDayIndex(day);

                        for (int i = startindex; i < endindex; i++)  //tabloya ekle
                        {


                            Button b = new Button();
                            b.Text = subj + " " + numb + " " + section;
                            //   b.Attributes.Add("onclick", "popWin()");
                            b.CssClass = "roundedbuttons";

                       //     Color clr = getCourseColor(subj + numb + section, courses);

                            int ind = 0;
                            ind = selectedCourses.IndexOf(subj + " " + numb + " " + section);
                            
                           


                            b.BackColor = CommonFunctions.colors[ind%40];
                            b.Enabled = true;
                            b.EnableViewState = true;
                            b.UseSubmitBehavior = false;
                            //    b.ID = b.Text;
                            b.UseSubmitBehavior = false;
                            b.Click += new EventHandler(btn_Click);

                            string cellPosition = (i).ToString() + ";" + (dayindex).ToString();
                            b.CommandArgument = cellPosition;
                            table_row_click = true;
                            table_filter_instructor_schedule.Rows[i].Cells[dayindex].Controls.Add(b);

                            pageControls.Add(new PageControls(i, dayindex, null, subj + " " + numb + " " + section, "button"));
                            //   ph.Controls.Add(b);
                            visualTable = table_filter_instructor_schedule;

                        }
                    }
                    catch (Exception exx)
                    {
                        Console.WriteLine(exx.Message);
                    }
                }
            }

        }
    }
}





                
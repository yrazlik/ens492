﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using ENS492.Classes;

namespace ENS492
{
    public partial class FileUploader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_upload_file_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FileUploadControl.FileName);
                    string fileExtension = Path.GetExtension(FileUploadControl.FileName);
                    string fileType = dropdown_file_type.SelectedValue.ToString();

                    if ((fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".xls") && fileType=="Schedule File")
                    {

                        FileUploadControl.SaveAs(Server.MapPath("~/") + filename);

                        InputFile inputFile = new InputFile(filename, Server.MapPath("~/") + filename);
                        FileToDBAdapter adapter = new FileToDBAdapter(inputFile);
                        adapter.writeToDB();
                        label_upload_status.Text = "File uploaded!";
                        label_upload_status.ForeColor = Color.Green;
                    }
                    else if ((fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".xls") && fileType == "Academic Blocks File")
                    {
                        FileUploadControl.SaveAs(Server.MapPath("~/") + filename);

                        InputFile inputFile = new InputFile(filename, Server.MapPath("~/") + filename);
                        FileToDBAdapter adapter = new FileToDBAdapter(inputFile);
                        adapter.formatAcademicBlocksFile();
                        label_upload_status.Text = "File uploaded!";
                        label_upload_status.ForeColor = Color.Green;
                        adapter.writeFormattedFileIntoDB();
                    }
                    else if ((fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".xls") && fileType == "Rooms")
                    {
                        FileUploadControl.SaveAs(Server.MapPath("~/") + filename);

                        InputFile inputFile = new InputFile(filename, Server.MapPath("~/") + filename);
                        FileToDBAdapter adapter = new FileToDBAdapter(inputFile);
                        adapter.writeRoomsToDB();

                    }
                    else
                    {
                        label_upload_status.Text = "Please choose a file of type csv, txt or xls";
                        label_upload_status.ForeColor = Color.Red;
                    }
                }
                catch (Exception ex)
                {
                    label_upload_status.Text = "The file could not be uploaded. The following error occured: " + ex.Message;
                    label_upload_status.ForeColor = Color.Red;
                }
            }

        }
    }
}
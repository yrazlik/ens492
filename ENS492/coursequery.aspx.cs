﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using ENS492.Classes;
using System.Drawing;

namespace ENS492
{
    public partial class coursequery : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
          //  if (!Page.IsPostBack)
          //  {

                DataTable subjects = new DataTable();

                try
                {
                    CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                    CommonFunctions.con.Open();
                }
                catch (Exception exx)
                {
                }


                try
                {
                    
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT [crn], [subj], [numb], [section] FROM Courses", CommonFunctions.con);
                    adapter.Fill(subjects);

                    foreach (DataRow dr in subjects.Rows)
                    {
                        string displayVal = dr["subj"].ToString() + " " + dr["numb"].ToString() + " " + dr["section"].ToString();


                        dropdown_course1.Items.Add(new ListItem(displayVal));
                        dropdown_course2.Items.Add(new ListItem(displayVal));
                        dropdown_course3.Items.Add(new ListItem(displayVal));
                        
                    }
                }
                catch (Exception ex)
                {
                    CommonFunctions.con.Close();
                    // Handle the error
                }



                // Add the initial item - you can add this even if the options from the
                // db were not successfully loaded

                CommonFunctions.con.Close();

        //    }

        }

        protected void button_conflict_check_button_Click(object sender, EventArgs e)
        {
            int numOfNonSelected=0;
            string dr1 = dropdown_course1.Text, dr2 = dropdown_course2.Text, dr3 = dropdown_course3.Text, subj1 = "", numb1 = "", section1 = "", subj2 = "", numb2 = "", section2 = "", subj3 = "", numb3 = "", section3 = "";
            if (dropdown_course1.Text == "")
                numOfNonSelected++;
            else
            {
                subj1 = dr1.Substring(dr1.IndexOf("-")+1,dr1.IndexOf(" ")-dr1.IndexOf("-")-1);
                dr1 = dr1.Substring(dr1.IndexOf(" ") + 1);
                String [] split = dr1.Split(' ');
                numb1 = split[0];
                section1 = split[1];
            }
            if (dropdown_course2.Text == "")
                numOfNonSelected++;
            else
            {
                subj2 = dr2.Substring(dr2.IndexOf("-") + 1, dr2.IndexOf(" ") - dr2.IndexOf("-") - 1);
                dr2 = dr2.Substring(dr2.IndexOf(" ") + 1);
                String[] split = dr2.Split(' ');
                numb2 = split[0];
                section2 = split[1];
            }
            if (dropdown_course3.Text == "")
                numOfNonSelected++;
            else
            {
                subj3 = dr3.Substring(dr3.IndexOf("-") + 1, dr3.IndexOf(" ") - dr3.IndexOf("-") - 1);
                dr3 = dr3.Substring(dr3.IndexOf(" ") + 1);
                String[] split = dr3.Split(' ');
                numb3 = split[0];
                section3 = split[1];
            }

            if (numOfNonSelected > 1)
            {
                label_status_label.Text = "You need to choose at least two courses";
                label_status_label.ForeColor = Color.Red;
            }
            else
            {
                List<String> blocks1 = new List<string>();
                List<String> blocks2 = new List<string>();
                List<String> blocks3 = new List<string>();
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();
                string coursename1 = subj1 + " " + numb1, coursename2 = subj2 + " " + numb2, coursename3 = subj3 + " " + numb3;

                if (coursename1 != null && coursename1 != "")
                {
                    SqlCommand cmd = new SqlCommand("SELECT [blockname] FROM academicblocks WHERE coursename=@coursename and coursesection=@coursesection", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@coursename", coursename1);
                    cmd.Parameters.AddWithValue("@coursesection", section1);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    
                    foreach (DataRow row in dt.Rows)
                    {
                        string block = row["blockname"].ToString();
                        if (!blocks1.Contains(block))
                        {
                            blocks1.Add(block);
                        }
                    }
                }

                if (coursename2 != null && coursename2 != "")
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT [blockname] FROM academicblocks WHERE coursename=@coursename2 and coursesection=@coursesection2", CommonFunctions.con);
                    cmd2.Parameters.AddWithValue("@coursename2", coursename2);
                    cmd2.Parameters.AddWithValue("@coursesection2", section2);
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    DataTable dt2 = new DataTable();
                    da2.Fill(dt2);
                    
                    foreach (DataRow row in dt2.Rows)
                    {
                        string block = row["blockname"].ToString();
                        if (!blocks2.Contains(block))
                        {
                            blocks2.Add(block);
                        }
                    }
                }

                if (coursename3 != null && coursename3 != "")
                {
                    SqlCommand cmd3 = new SqlCommand("SELECT [blockname] FROM academicblocks WHERE coursename=@coursename3 and coursesection=@coursesection3", CommonFunctions.con);
                    cmd3.Parameters.AddWithValue("@coursename3", coursename3);
                    cmd3.Parameters.AddWithValue("@coursesection3", section3);
                    SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                    DataTable dt3 = new DataTable();
                    da3.Fill(dt3);
                    
                    foreach (DataRow row in dt3.Rows)
                    {
                        string block = row["blockname"].ToString();
                        if (!blocks3.Contains(block))
                        {
                            blocks3.Add(block);
                        }
                    }
                }

                if (blocks1.Count>0 && blocks2.Count>0 && blocks1.Intersect(blocks2).Any()) //if course 1 and course 2 are in same block
                {
                    List<String> intersectedBlocks = blocks1.Intersect(blocks2).ToList();
                    string intersectedBlockNames = "";
                    foreach (String s in intersectedBlocks)
                    {
                        intersectedBlockNames += " "+ s;
                    }

                    label_status_label.Text = "Sorry there cannot be any conflict because, the courses " + subj1 + " " + numb1 + " " + section1 + " and " + subj2 + " " + numb2 + " " + section2 + " are in the same academic blocks called" + intersectedBlockNames + ".";
                    label_status_label.ForeColor = Color.Red;

                    visualizeBlocks(intersectedBlocks);
                }
                else if (blocks2.Count > 0 && blocks3.Count > 0 && blocks2.Intersect(blocks3).Any()) //if course 3 and course 2 are in same block
                {
                    List<String> intersectedBlocks = blocks2.Intersect(blocks3).ToList();
                    string intersectedBlockNames = "";
                    foreach (String s in intersectedBlocks)
                    {
                        intersectedBlockNames += " " + s;
                    }

                    label_status_label.Text = "Sorry there cannot be any conflict because, the courses " + subj2 + " " + numb2 + " " + section2 + " and " + subj3 + " " + numb3 + " " + section3 + " are in the same academic blocks called" + intersectedBlockNames + ".";
                    label_status_label.ForeColor = Color.Red;


                }
                else if (blocks1.Count > 0 && blocks3.Count > 0 && blocks1.Intersect(blocks3).Any()) //if course 1 and course 3 are in same block
                {
                    List<String> intersectedBlocks = blocks1.Intersect(blocks3).ToList();
                    string intersectedBlockNames = "";
                    foreach (String s in intersectedBlocks)
                    {
                        intersectedBlockNames += " " + s;
                    }

                    label_status_label.Text = "Sorry there cannot be any conflict because, the courses " + subj1 + " " + numb1 + " " + section1 + " and " + subj3 + " " + numb3 + " " + section3 + " are in the same academic blocks called" + intersectedBlockNames + ".";
                    label_status_label.ForeColor = Color.Red;
                }
                else
                {
                    label_status_label.Text = "Courses can conflict";
                    label_status_label.ForeColor = Color.Green;
                }

                CommonFunctions.con.Close();

            }

        }

        public void visualizeBlocks(List<String> intersectedBlocks)
        {
            Table tableVisual = table_visualize_blocks;
          //  tableVisual.Rows.Clear();
            try
            {
                    CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
                CommonFunctions.con.Open();

            }
            catch (Exception ex)
            {
            }
            int index = 0;
            
            foreach (String blockName in intersectedBlocks)
            {
                try
                {
                    DataTable subjects = new DataTable();

                    DataTable dt = new DataTable();
                    
                    SqlCommand cmd = new SqlCommand("SELECT [coursename], [coursesection] FROM academicblocks where blockname=@blockname AND term=@term", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@blockname", blockName);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    Table t = new Table();
                    t.CssClass = "singletable";
                //    t.Attributes.Add("CssClass", "singletable");
                    t.BorderWidth = 1;
                //    t.GridLines = GridLines.Horizontal;

                    TableRow row = new TableRow();
                    TableCell cl = new TableCell();
               //     cl.BackColor=Color.FromArgb(240,232,146);
                    cl.Text = blockName;
                    row.Cells.Add(cl);
                    t.Rows.Add(row);
                    
                    
                   
            
                    foreach (DataRow r in dt.Rows)
                    {
                        TableRow tr = new TableRow();
                        TableCell c = new TableCell();
                        c.Text = r["coursename"] + " " + r["coursesection"];
                  //      c.BackColor = Color.FromArgb(161, 240, 204);
                        tr.Cells.Add(c);
                         t.Rows.Add(tr);
                    }

                    tableVisual.Rows[0].Cells[index].Controls.Add(t);
                    
                }
                catch (Exception e)
                {
                    CommonFunctions.con.Close();
                }
                index++;
                CommonFunctions.con.Close();

            }

        }
    }
}
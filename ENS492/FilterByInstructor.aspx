﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilterByInstructor.aspx.cs" Inherits="ENS492.FilterByInstructor"
    MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true"></ajaxToolkit:ToolkitScriptManager>
        <asp:SqlDataSource ID="Database" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT DISTINCT [name] FROM Instructors ORDER BY [name]"></asp:SqlDataSource>
        <br />
        <table style="width:100%;">
            <tr>
                <td class="style5">
        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#660066" 
            Text="Select the instructor from list to view weekly schedule:"></asp:Label>
                </td>
                <td class="style8">
        <asp:DropDownList CssClass="CustomComboBoxStyle" ID="dropdown_all_instructors" runat="server" DataSourceID="Database"
            DataTextField="name" DataValueField="name">
        </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
                </td>
                <td>
        <asp:Button CssClass="showbutton" ID="button_showInstructorSchedule" runat="server" Text="View Schedule By Instructor"
            OnClick="button_showInstructorSchedule_Click" Width="168px" />
                </td>
            </tr>
            <tr>
                <td >
        <asp:Label ID="label_comma_separated_courses" runat="server" Font-Bold="True" ForeColor="#660066" 
            Text="Or select more than one course:"></asp:Label>
                </td>
                <td >
                <div style="line-height:inherit; text-align:left; width:252px; height:116px; overflow:scroll;">
                    <asp:CheckBoxList style="line-height:normal"  ID="checkboxlist_courses" 
                        runat="server" >
                    </asp:CheckBoxList>
                </div>
                </td>
                <td >
        <asp:Button CssClass="showbutton" ID="button_showScheculeByCoursename" runat="server" Text="View Schedule By Course Name"
             Width="167px" onclick="button_showScheculeByCoursename_Click" />
                </td>
            </tr>
            <tr>
                <td class="style5">
                    &nbsp;</td>
                <td class="style8">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <asp:Table CssClass="tables" ID="table_filter_instructor_schedule" 
            runat="server" BorderWidth="1px"
            GridLines="Both">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" Width="145">HRS/DAY</asp:TableCell>
                <asp:TableCell Width="145">MON</asp:TableCell>
                <asp:TableCell Width="145">TUE</asp:TableCell>
                <asp:TableCell Width="145">WED</asp:TableCell>
                <asp:TableCell Width="145">THU</asp:TableCell>
                <asp:TableCell Width="145">FRI</asp:TableCell>
                <asp:TableCell Width="145">SAT</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell BackColor="#cece3b">8:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>9:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>10:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>11:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>12:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>13:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>14:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>15:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>16:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>17:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>18:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>19:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>20:40</asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Button ID="btn" runat="server" OnClick="btn_Click" Text="Button" Visible="False" />
        <br />
    </div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnPopUp" runat="server" Text="Button" Height="58px" 
                Width="350px" onclick="btnPopUp_Click" />
            <ajaxToolkit:ModalPopupExtender ID="btnPopUp_ModalPopupExtender" runat="server" 
                DynamicServicePath="" Enabled="True" TargetControlID="btnPopUp" 
                BackgroundCssClass="modalBackground" PopupControlID="PanelModal" CancelControlID="close">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="PanelModal" runat="server" style="display:none; background:white; width:80%; height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3 id=myModalLabel> Information</h3>
                </div>
                <div class="modal-body">
                    <asp:Label ID="label_current_schedule" runat="server" Text="Label"></asp:Label>
                    <asp:DropDownList ID="dropdown_available_rooms" runat="server" />
                    <asp:Table ID="table_visualize_blocks" runat="server">
                        <asp:TableRow>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                            <asp:TableCell style="vertical-align:top;" Width="80"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
                <div class="modal-footer">
                    <button id="close" class="btn"  data-dismiss="modal" aria-hidden="true">Close</button>
                    <asp:Button ID="ModalOKButton" runat="server" Text="Submit Changes" 
                        onclick="ModalOKButton_Click" CssClass="btn btn-primary" CausesValidation="true" />
                    
                </div>

           
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style3
        {
            width: 418px;
            height: 22px;
        }
        .style4
        {
            height: 22px;
        }
        .style5
        {
            width: 418px;
        }
        .style7
        {
            height: 22px;
            width: 303px;
        }
        .style8
        {
            width: 303px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">
        
            <p>
            <img src="Pictures/instweekly.jpg" alt="" style="height: 39px; width: 281px" />
            </p>
        
        </asp:Content>


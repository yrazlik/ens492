﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492
{
    public class AcademicBlock
    {
        public List<BlockCourse> blockCourses;
        String blockName;

        public AcademicBlock(string blockName)
        {
            this.blockName = blockName;
        }

        public void setBlockName(string blockName)
        {
            this.blockName = blockName;
        }

        public string getBlockName()
        {
            return blockName;
        }

        






        

    }

    public class BlockCourse
    {
        string courseName;
        string courseSection;

        public BlockCourse(string courseName, string courseSection)
        {
            this.courseName = courseName;
            this.courseSection = courseSection;
        }

        public BlockCourse(string courseName)
        {
            this.courseName = courseName;
        }

        public string getCourseName()
        {
            return courseName;
        }

        public string getCourseSection()
        {
            return courseSection;
        }

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploader.aspx.cs" Inherits="ENS492.FileUploader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:FileUpload ID="FileUploadControl" runat="server" />
&nbsp;<asp:Button ID="button_upload_file" runat="server" 
            onclick="button_upload_file_Click" Text="Upload" Width="68px" />
    
        <br />
        <asp:DropDownList ID="dropdown_file_type" runat="server">
            <asp:ListItem>Schedule File</asp:ListItem>
            <asp:ListItem>Academic Blocks File</asp:ListItem>
            <asp:ListItem>Rooms</asp:ListItem>
        </asp:DropDownList>
    
        <br />
        <asp:Label ID="Label1" runat="server" Text="Upload Status:  "></asp:Label>
        <asp:Label ID="label_upload_status" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>

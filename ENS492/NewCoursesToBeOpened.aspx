﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewCoursesToBeOpened.aspx.cs"
    Inherits="ENS492.NewCoursesToBeOpened" MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <asp:FileUpload ID="uploader" runat="server" />
        <asp:Button ID="button_upload" runat="server" OnClick="button_upload_Click" Text="Upload"
            Width="96px" />
        <br />
        <br />
        <asp:Label ID="lbl_remove" runat="server" Text="Please select the courses that will be removed from their academic blocks:"
            Visible="False"></asp:Label>
        <br />
        <div id="ScrollList" style="height: 200px; width: 450px; overflow: auto">
            <asp:GridView Height="100px" ID="showCourses" runat="server" AutoGenerateColumns="False"
                BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="4"
                ForeColor="Black" GridLines="Vertical">
                <RowStyle BackColor="#F7F7DE" />
                <Columns>
                    <asp:TemplateField HeaderText="Remove?">
                        <ItemTemplate>
                            <asp:CheckBox ID="checkbox" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CourseCode" HeaderText="Course Code" />
                    <asp:BoundField DataField="CourseNumber" HeaderText="Course Number" />
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#7B769C" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#564D94" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>
        <asp:Button ID="button_submit_changes" runat="server" OnClick="button_submit_changes_Click"
            Text="Next" Width="286px" Visible="False" />
        <br />
        <br />
        <table bgcolor="White" class="plaintable" width="100%">
            <tr>
                <td class="style1">
                    <div id="Scrollist2" style="height: 200px; overflow: auto">
                        <br />
                        <asp:GridView OnRowCommand="showCoursesToBeAdded_RowCommand" ID="showCoursesToBeAdded"
                            runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="White"
                            BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                            <RowStyle BackColor="#F7F7DE" />
                            <Columns>
                                <asp:BoundField DataField="CourseCode" HeaderText="Course Code" />
                                <asp:BoundField DataField="CourseNumber" HeaderText="Course Number" />
                                <asp:TemplateField HeaderText="Suggestion">
                                    <ItemTemplate>
                                        <asp:Button runat="server" ID="button_advise" Text="Show Suggestion" CommandName="ShowSuggestion"
                                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#7B769C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#564D94" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </div>
                </td>
                <td class="style2">
                    <asp:Table ID="table_visualize_blocks" runat="server">
                        <asp:TableRow>
                            <asp:TableCell Width="80">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                    GridLines="Vertical">
                                    <RowStyle BackColor="#F7F7DE" />
                                    <Columns>
                                        <asp:BoundField DataField="BlockCourses" HeaderText="Block Courses" />
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#7B769C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#564D94" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                            <asp:TableCell Width="80"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <div id="Scrollist3" style="height: 200px; overflow: auto">
                        <asp:GridView ID="blockSuggestions" runat="server" AutoGenerateColumns="False" BackColor="White"
                            BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                            GridLines="Vertical">
                            <RowStyle BackColor="#F7F7DE" />
                            <Columns>
                                <asp:TemplateField HeaderText="Add to Block?">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="checkboxAdd" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BlockName" HeaderText="Block Name" />
                                <asp:BoundField DataField="BlockCourses" HeaderText="Block Courses">
                                    <ItemStyle Width="300px"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#7B769C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#564D94" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </div>
                </td>
                <td>
                    <asp:Label ID="lbl_message" runat="server" ForeColor="Lime" Text="Course Added To Selected Blocks Succesfully"
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Or create a new block called: " Visible="False"></asp:Label>
                    &nbsp;
                    <asp:TextBox ID="TextBox1" runat="server" Height="26px" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="button_finish_editing" runat="server" OnClick="button_finish_editing_Click"
                        Text="Submit Changes" Visible="False" />
                </td>
                <td>
                    <asp:Button ID="button_done_adding" runat="server" OnClick="button_done_adding_Click"
                        Text="Add" Width="613px" Visible="False" />
                </td>
            </tr>
        </table>
    </div>
    <asp:Label ID="lbl_finish" runat="server" ForeColor="#003300" Text=" Academic Blocks file created. You can find it at C:\NewAcademicBlocks.txt"
        Visible="False" Font-Bold="True"></asp:Label>
    &nbsp;</form>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style3
        {
            width: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
    <p>
        <img src="Pictures/instweekly.jpg" alt="" style="height: 39px; width: 281px" />
    </p>
</asp:Content>

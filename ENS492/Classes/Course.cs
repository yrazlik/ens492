﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace ENS492.Classes
{
    public class Course
    {
        private string subj, numb, section, crn,term;
        private Color color;
        public Room classroom;
       

        public Course(string day, string starthour, string endhour, string building, string room)
        {
            this.starthour = starthour;
            this.endhour = endhour;
            this.day = day;
            this.building = building;
            this.room = room;
        }

        public Course(string subj, string numb, string section, string starthour, string endhour, string day, string building, string room)
        {
            this.subj = subj;
            this.numb = numb;
            this.section = section;
            this.starthour = starthour;
            this.endhour = endhour;
            this.day = day;
            this.building = building;
            this.room = room;
        }


        public string getSubj()
        {
            return subj;
        }

        public string getNumb()
        {
            return numb;
        }

        public string getStartHour()
        {
            return starthour;
        }

        public string getEndHour()
        {
            return endhour;
        }

        public string getDay()
        {
            return day;
        }

        public string getBuilding()
        {
            return building;
        }

        public string getRoom()
        {
            return room;
        }

        public string getTerm()
        {
            return term;
        }

        public Course(string subj, string numb)
        {
            this.subj = subj;
            this.numb = numb;
        }

        public Course(string subj, string numb, string section, string crn, Color color)
        {
            this.subj = subj;
            this.numb = numb;
            this.section = section;
            this.crn = crn;
            this.color = color;
        }

        public Color getColor()
        {
            return color;
        }

        public string getCourseName()
        {
            return subj + numb + section;
        }

        private string starthour, endhour, day, building, room;
    }
}
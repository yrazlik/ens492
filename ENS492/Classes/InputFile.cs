﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492.Classes
{
    public class InputFile
    {
        private string fileName;
        private string filePath;

        public InputFile(string fileName, string filePath)
        {
            this.fileName = fileName;
            this.filePath = filePath;
        }

        public string getFileName()
        {
            return fileName;
        }

        public string getFilePath()
        {
            return filePath;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;

namespace ENS492.Classes
{

    public interface DBWriter
    {
        void readFile();
        void writeToDB();
    }

    public class FileToDBAdapter : DBWriter
    {
        private InputFile file;

        public FileToDBAdapter(InputFile file)
        {
            this.file = file;
        }

        public void readFile()
        {

        }
        int whichline = 0;


        public void writeRoomsToDB()
        {
            var reader = new StreamReader(File.OpenRead(file.getFilePath()));
            CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
            try
            {
                CommonFunctions.con.Open();
                CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
            }
            catch(Exception ex)
            {
            }
            
            SqlCommand cmd;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                var values = line.Split(';');

                string building = values[0];
                string room = values[2];
                string description = values[3];
                string type = values[4];
                string capacity = values[5];

                try
                {
                    cmd = new SqlCommand("insert into allrooms  (building,room,description,type,capacity,pc,projector,soundsystem,whiteboard,window,screen,instructorinternetdataport) values(@building,@room,@description,@type,@capacity,'yes','yes','yes','yes','yes','yes','yes')", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@building", building);
                    cmd.Parameters.AddWithValue("@room", room);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@type", type);
                    cmd.Parameters.AddWithValue("@capacity", capacity);
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex)
                {

                }

            }
        }

        public void writeFormattedFileIntoDB()
        {
            List<AcademicBlock> academicBlocks = new List<AcademicBlock>();
            StreamReader reader = new StreamReader("C:\\Formatted_Academic_Blocks.txt");
            string line, prevLine=";;;";
            int index = 0;
            int lineNum = 0;
            line = reader.ReadLine();
            while (!reader.EndOfStream)
            {                
                
                if(prevLine.Contains(";;;"))
                {
                    index = academicBlocks.Count;
                    AcademicBlock block = new AcademicBlock(line);
                    block.blockCourses = new List<BlockCourse>();
                    academicBlocks.Add(block);
                    
                }
                else
                {
                    if (line != ";;;;;;;;;;")
                    {
                        String[] splittedLine = line.Split('+');
                        BlockCourse course = new BlockCourse(splittedLine[0], splittedLine[1]);
                        academicBlocks[index].blockCourses.Add(course);
                    }
                }

                prevLine = line;
                line = reader.ReadLine();
                lineNum++;

            }

            CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
            CommonFunctions.con.Open();
            SqlCommand cmd;

            foreach(AcademicBlock a in academicBlocks)
            {
                String blockname = a.getBlockName();
                foreach (BlockCourse b in a.blockCourses)
                {
                    string coursename=b.getCourseName();
                    string coursesection=b.getCourseSection();
                    cmd = new SqlCommand("insert into academicblocks (blockname,coursename,coursesection, term) values(@blockname,@coursename,@coursesection, @term)", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@blockname", blockname);
                    cmd.Parameters.AddWithValue("@coursename", coursename);
                    cmd.Parameters.AddWithValue("@coursesection", coursesection);
                    cmd.Parameters.AddWithValue("@term", CommonFunctions.currentTerm);
                    cmd.ExecuteNonQuery();
                }
            }

            CommonFunctions.con.Close();

        }

        public void formatAcademicBlocksFile()
        {
            string allBlocksAndCourses = "";

            List<AcademicBlock> academicBlocks = new List<AcademicBlock>();
            
            var reader = new StreamReader(File.OpenRead(file.getFilePath()));
            bool blockName = true;
            int i = 0;
            string line = reader.ReadLine();
            string [] values = line.Split(';');
            int startIndex = 0;
            StreamWriter wr = new StreamWriter("C:\\HATA.txt");
            string prevLine ="";

            while (!reader.EndOfStream)
            {


                try
                {
                    var prevValues = prevLine.Split(';');

                    foreach (string s in prevValues)
                    {
                        if (s != "")
                        {
                            blockName = false;
                            break;
                        }
                    }


                    if (blockName)
                    {
                        startIndex = academicBlocks.Count;
                        foreach (string s in values)
                        {
                            if (s != "")
                            {
                                AcademicBlock block = new AcademicBlock(s);
                                block.blockCourses = new List<BlockCourse>();
                                academicBlocks.Add(block);
                                i++;
                            }

                        }

                    }
                    else
                    {
                        int j = startIndex;
                        for (int k = 0; k < values.Length; k++)
                        {
                            string course = values[k];
                            if (course != "")
                            {
                                string section = values[++k];
                                BlockCourse blockCourse = new BlockCourse(course, section);
                                academicBlocks[j+k/3].blockCourses.Add(blockCourse);
                              //  j++;
                            }

                        }

                    }

                    blockName = true;
                    prevLine = line;
                    string nextLine = reader.ReadLine();
                    var nextValues = nextLine.Split(';');
                    line = nextLine;
                    values = nextValues;
                 /*   foreach (string s in nextValues)
                    {
                        if (s != "")
                        {
                            blockName = false;
                            break;
                        }
                    }*/




                    Console.Write("");
                    whichline++;
                }
                catch (Exception e)
                {
                    reader.ReadLine();
                    
                    wr.WriteLine(whichline);
                    Console.WriteLine("HATAAAAAA: " + whichline);
                }
            }

            StreamWriter w = new StreamWriter("C:\\Formatted_Academic_Blocks.txt");
            
            foreach (AcademicBlock a in academicBlocks)
            {
                w.WriteLine(a.getBlockName());
                foreach (BlockCourse b in a.blockCourses)
                {
                    w.WriteLine(b.getCourseName() + "+" + b.getCourseSection());
                }
                w.WriteLine(";;;;;;;;;;");

            }
            w.Close();

        }

        public int getHourByIndex(int index)
        {
            if (index == 1)
                return 8;
            else if (index == 2)
                return 9;
            else if (index == 3)
                return 10;
            else if (index == 4)
                return 11;
            else if (index == 5)
                return 12;
            else if (index == 6)
                return 13;
            else if (index == 7)
                return 14;
            else if (index == 8)
                return 15;
            else if (index == 9)
                return 16;
            else if (index == 10)
                return 17;
            else if (index == 11)
                return 18;
            else if (index == 12)
                return 19;
            else if (index == 13)
                return 20;

            return -1;
        }

        public int getHourIndex(int hour)
        {
            if (hour == 8)
                return 1;
            else if (hour == 9)
                return 2;
            else if (hour == 10)
                return 3;
            else if (hour == 11)
                return 4;
            else if (hour == 12)
                return 5;
            else if (hour == 13)
                return 6;
            else if (hour == 14)
                return 7;
            else if (hour == 15)
                return 8;
            else if (hour == 16)
                return 9;
            else if (hour == 17)
                return 10;
            else if (hour == 18)
                return 11;
            else if (hour == 19)
                return 12;
            else if (hour == 20)
                return 13;

            return -1;
        }

        public void writeToDB()
        {
            var reader = new StreamReader(File.OpenRead(file.getFilePath()));
            CommonFunctions.con.ConnectionString = CommonFunctions.getConnectionString();
            CommonFunctions.con.Open();
            reader.ReadLine();
            SqlCommand cmd;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                var values = line.Split(';');

                string term = values[0];
                string crn = values[1] ;
                string subj = values[2];
                string numb = values[3];
                string section = values[4];
                string title = values[5];
                string coll = values[6];
                string ssts = values[7];
                string sch = values[8];
                string bldg = values[9];
                string room = values[10];
                string capacity = values[11];
                string hrsweek = values[12];
                string mon = values[13];
                string tue = values[14];
                string wed = values[15];
                string thu = values[16];
                string fri = values[17];
                string sat = values[17];
                string begin = values[19];  //1040 biçiminde
                string end = values[20];
                string instructor = values[21] ;
                string day = "";
                if (mon != "")
                    day = mon;
                else if (tue != "")
                    day = tue;
                else if (wed != "")
                    day = wed;
                else if (thu != "")
                    day = thu;
                else if (fri != "")
                    day = fri;
                else if (sat != "")
                    day = sat;

                try
                {                    

                    cmd = new SqlCommand("insert into Courses (crn,term,subj,numb,section,title,coll,ssts,sch,hrsweek,instructor,capacity) values(@crn,@term,@subj,@numb,@section,@title,@coll,@ssts,@sch,@hrsweek,@instructor,@capacity)", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@crn", crn);
                    cmd.Parameters.AddWithValue("@term", term);
                    cmd.Parameters.AddWithValue("@subj", subj);
                    cmd.Parameters.AddWithValue("@numb", numb);
                    cmd.Parameters.AddWithValue("@section", section);
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@coll", coll);
                    cmd.Parameters.AddWithValue("@ssts", ssts);
                    cmd.Parameters.AddWithValue("@sch", sch);
                    cmd.Parameters.AddWithValue("@hrsweek", hrsweek);
                    cmd.Parameters.AddWithValue("@instructor", instructor);
                    cmd.Parameters.AddWithValue("@capacity", capacity);
                    cmd.ExecuteNonQuery();

                }
                catch (Exception e)
                {

                }

                try
                {
                    cmd = new SqlCommand("insert into Instructors (name) values(@instructor)", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@instructor", instructor);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {

                }

                try
                {
                    string startH = "", endH = "";
                    if (begin.Length == 3)
                    {
                        startH = begin.Substring(0, 1);
                    }
                    else if (begin.Length == 4)
                    {
                        startH = begin.Substring(0, 2);
                    }

                    if (end.Length == 3)
                    {
                        endH = end.Substring(0, 1);
                    }
                    else if (end.Length == 4)
                    {
                        endH = end.Substring(0, 2);
                    }

                    int startInt = Convert.ToInt32(startH), endInt = Convert.ToInt32(endH);

                    for (int i = startInt; i < endInt; i++)
                    {
                        cmd = new SqlCommand("insert into Timetable (starthour,endhour,crn,day, term,building,room,subj,numb,section) values(@starthour,@endhour,@crn,@day,@term,@building,@room,@subj,@numb,@section)", CommonFunctions.con);
                        cmd.Parameters.AddWithValue("@starthour", i.ToString() + "40");
                        cmd.Parameters.AddWithValue("@endhour", (i + 1).ToString() + "30");
                        cmd.Parameters.AddWithValue("@crn", crn);
                        cmd.Parameters.AddWithValue("@day", day);
                        cmd.Parameters.AddWithValue("@term", term);
                        cmd.Parameters.AddWithValue("@building", bldg);
                        cmd.Parameters.AddWithValue("@room", room);
                        cmd.Parameters.AddWithValue("@subj", subj);
                        cmd.Parameters.AddWithValue("@numb", numb);
                        cmd.Parameters.AddWithValue("@section", section);
                        cmd.ExecuteNonQuery();
                    }

                    
                   
                }
                catch (Exception e)
                {

                }

                try
                {
                    cmd = new SqlCommand("insert into Rooms (building,room,capacity,crn,term) values(@building,@room,@capacity,@crn, @term)", CommonFunctions.con);
                    cmd.Parameters.AddWithValue("@building", bldg);
                    cmd.Parameters.AddWithValue("@room", room);
                    cmd.Parameters.AddWithValue("@capacity", capacity);
                    cmd.Parameters.AddWithValue("@crn", crn);
                    cmd.Parameters.AddWithValue("@term", term);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {

                }

                
            }

            CommonFunctions.con.Close();


        }



    }
}
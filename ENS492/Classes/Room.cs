﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492.Classes
{
    public class Room
    {
        private int capacity;
        private string building, room;


        public Room(string building, string room, int capacity)
        {
            this.room = room;
            this.capacity = capacity;
            this.building = building;
        }

        public string getRoom()
        {
            return room;
        }

        public string getBuilding()
        {
            return building;
        }


        public int getCapacity()
        {
            return capacity;
        }
    }
}
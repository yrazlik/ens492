﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492.Classes
{
    public class University
    {
        private University university;

        public University getInstance()
        {
            if(university == null)
            {
                university = new University();
                return university;
            }
            return university;
        }

        private string universityName = "Sabanci University";
        private string connectionString;

        public void setConnectionString(string con)
        {
            connectionString = con;
        }

        public string getUniversityName()
        {
            return universityName;
        }

    }
}
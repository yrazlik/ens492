﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;

namespace ENS492.Classes
{
    public class CommonFunctions
    {
        public static string currentTerm = "201101";
        public static string prevTerm = "201002";
        public static string prevYearSameTerm = "201001";
        public static ScriptManager manager = new ScriptManager();
        public static SqlConnection con = new SqlConnection();
        static string connectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=d:\\users\\suuser\\documents\\visual studio 2010\\Projects\\ENS492\\ENS492\\App_Data\\Database.mdf;Integrated Security=True;User Instance=True";
        public static string currentUserType="", currentUserName;

        public static string getConnectionString()
        {
            return connectionString;
        }

        public static Color[] colors = {Color.Aqua, Color.Aquamarine, Color.Azure, Color.Beige, Color.Bisque,
                                           Color.BlanchedAlmond, Color.Blue, Color.BlueViolet, Color.Brown, Color.BurlyWood, Color.CadetBlue, Color.Chartreuse,
                                           Color.Chocolate, Color.Coral, Color.AntiqueWhite, Color.CornflowerBlue, Color.Cornsilk, Color.Crimson, Color.Cyan, Color.DarkBlue,
                                           Color.DarkCyan, Color.DarkGoldenrod, Color.DarkGray, Color.DarkGreen, Color.DarkKhaki, Color.DarkMagenta, Color.DarkOliveGreen,
                                           Color.DarkOrange, Color.DarkOrchid, Color.DarkRed, Color.DarkSalmon, Color.DarkSeaGreen, Color.DarkSlateBlue, Color.DarkSlateGray,
                                           Color.DarkTurquoise, Color.DarkViolet, Color.DeepPink, Color.DeepSkyBlue, Color.AliceBlue, Color.DimGray, Color.DodgerBlue};


    }
}
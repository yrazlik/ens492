﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492.Classes
{
    public abstract class Member
    {
        protected string name;
        protected string ID;
        public abstract string getName();
        public abstract string getID();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492.Classes
{
    public class Instructor : Member
    {
        
        private List<Course> courses = new List<Course>();

        public Instructor(string name)
        {
            this.name = name;
        }

        public override string getName()
        {
            return name;
        }

        public override string getID()
        {
            return ID;
        }

        public void addCourse(Course c)
        {
            courses.Add(c);
        }

        public List<Course> getCourses()
        {
            return courses;
        }
    }
}
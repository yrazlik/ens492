﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowWhichSetOFCoursesCanConflict.aspx.cs"
    Inherits="ENS492.ShowWhichSetOFCoursesCanConflict" MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            
            <br />
            <br />
            <table style="width:100%; margin-right: 0px;">
                <tr>
                    <td class="style6">
                        <strong>Set of Courses</strong></td>
                    <td class="style5">
                        <strong>Course Code</strong></td>
                    <td class="style15">
                        <strong>Course Number</strong></td>
                    <td class="style4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style10">
                        1st set of courses:</td>
                    <td class="style8">
                        <asp:DropDownList AppendDataBoundItems="true" ID="dropdown_first_set_of_courses" runat="server" 
                            DataSourceID="SqlDataSource1" DataTextField="subj" DataValueField="subj">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                            SelectCommand="SELECT DISTINCT subj FROM Courses order by subj">
                        </asp:SqlDataSource>
                    </td>
                    <td class="style16">
                        <asp:DropDownList 
                            ID="dropdown_first_set_of_course_numbers" runat="server">
                                <asp:ListItem>0xx</asp:ListItem>
                            <asp:ListItem>1xx</asp:ListItem>
                            <asp:ListItem>2xx</asp:ListItem>
                            <asp:ListItem>3xx</asp:ListItem>
                            <asp:ListItem>4xx</asp:ListItem>
                            <asp:ListItem>5xx</asp:ListItem>
                            <asp:ListItem>6xx</asp:ListItem>
                            <asp:ListItem>7xx</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style9">
                        <asp:Label ID="label_error" runat="server" Font-Bold="True" ForeColor="#FF3300" 
                            Text="You need to select at least 2 course codes and 2 course numbers" 
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style10">
                        2nd set of courses:</td>
                    <td class="style8">
                        <asp:DropDownList AppendDataBoundItems="true" ID="dropdown_second_set_of_courses" runat="server" 
                            DataSourceID="SqlDataSource2" DataTextField="subj" DataValueField="subj">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                            SelectCommand="select distinct subj from Courses order by subj">
                        </asp:SqlDataSource>
                    </td>
                    <td class="style16">
                        <asp:DropDownList ID="dropdown_second_set_of_course_numbers" runat="server">
                            <asp:ListItem>0xx</asp:ListItem>
                            <asp:ListItem>1xx</asp:ListItem>
                            <asp:ListItem>2xx</asp:ListItem>
                            <asp:ListItem>3xx</asp:ListItem>
                            <asp:ListItem>4xx</asp:ListItem>
                            <asp:ListItem>5xx</asp:ListItem>
                            <asp:ListItem>6xx</asp:ListItem>
                            <asp:ListItem>7xx</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style9">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style14">
                        3rd set of courses:</td>
                    <td class="style12">
                        <asp:DropDownList DataSourceID="SqlDataSource3" AppendDataBoundItems="true"
                            ID="dropdown_third_set_of_courses" runat="server" DataTextField="subj" 
                            DataValueField="subj">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                            SelectCommand="select distinct subj from Courses order by subj">
                        </asp:SqlDataSource>
                    </td>
                    <td class="style17">
                        <asp:DropDownList ID="dropdown_third_set_of_course_numbers" runat="server">
                            <asp:ListItem>0xx</asp:ListItem>
                            <asp:ListItem>1xx</asp:ListItem>
                            <asp:ListItem>2xx</asp:ListItem>
                            <asp:ListItem>3xx</asp:ListItem>
                            <asp:ListItem>4xx</asp:ListItem>
                            <asp:ListItem>5xx</asp:ListItem>
                            <asp:ListItem>6xx</asp:ListItem>
                            <asp:ListItem>7xx</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style13">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Button ID="button_submit" runat="server" onclick="button_submit_Click" 
                            Text="Submit" />
                    </td>
                    <td class="style2">
                        &nbsp;</td>
                    <td class="style18">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style3" colspan="4">
                        <asp:Label ID="label_courses_that_can_conflict" runat="server" 
                            Font-Bold="False" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style2
        {
            width: 121px;
        }
        .style3
        {
            font-weight: 700;
        }
        .style4
        {
            color: #660066;
            height: 57px;
        }
        .style5
        {
            width: 121px;
            color: #660033;
            height: 57px;
        }
        .style6
        {
            width: 173px;
            font-weight: 700;
            height: 57px;
        }
        .style8
        {
            width: 121px;
            height: 59px;
        }
        .style9
        {
            height: 59px;
        }
        .style10
        {
            width: 173px;
            font-weight: 700;
            height: 59px;
        }
        .style12
        {
            width: 121px;
            height: 58px;
        }
        .style13
        {
            height: 58px;
        }
        .style14
        {
            width: 173px;
            font-weight: 700;
            height: 58px;
        }
        .style15
        {
            color: #660066;
            height: 57px;
            width: 259px;
        }
        .style16
        {
            height: 59px;
            width: 259px;
        }
        .style17
        {
            height: 58px;
            width: 259px;
        }
        .style18
        {
            width: 259px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
    <p>
        <img src="Pictures/sabanci_main_menu.gif" alt="" style="height: 35px; width: 200px" />
    </p>
</asp:Content>

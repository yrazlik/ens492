﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENS492
{
    public class PageControls
    {
        public int xPos, yPos;
        public string ID, Text, type,argument;

        

        public PageControls(int xPos, int yPos, string ID, string Text, string type)
        {
            this.xPos = xPos;
            this.yPos = yPos;
            this.ID = ID;
            this.Text = Text;
            this.type = type;
        }

        public PageControls(int xPos, int yPos, string ID, string Text, string type,string argument)
        {
            this.xPos = xPos;
            this.yPos = yPos;
            this.ID = ID;
            this.Text = Text;
            this.type = type;
            this.argument = argument;
        }

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="ENS492.Welcome"
    MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <p>
            &nbsp;</p>
        <p>
            <table style="width: 100%;">
                <tr>
                    <td class="style3">
                        &nbsp;&nbsp;
                        <a class="submenulinktext2 " 
        href="" 
        onblur="window.status=''; return true" 
        onfocus="window.status='Student Records'; return true" 
        onmouseout="window.status=''; return true" 
        onmouseover="window.status='Student Records'; return true">
    <img border="0" class="headerImg" height="12" hspace="0" 
        name="sabanci_bullet_red_small" 
        src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
        title="" vspace="0" width="4" /></a>
                        </td>
                    <td>
        <asp:LinkButton ID="linkbutton_select_term" runat="server"  Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black" onclick="linkbutton_select_term_Click" 
                           >Select Term</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style3">
                        &nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small0" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
                        <asp:LinkButton ID="button_show_weekly_schedule" runat="server" 
                            OnClick="button_show_weekly_schedule_Click" Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black">Show Weekly Schedule</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style3">
                        &nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small1" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
        <asp:LinkButton ID="button_show_academic_blocks" runat="server" Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black" 
                            onclick="button_show_academic_blocks_Click">Show Academic Blocks</asp:LinkButton>
                    </td>
                    <td>
                         
                </tr>
                <tr>
                    <td class="style3">
                        &nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small1" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
        <asp:LinkButton ID="linkbutton_check_conflict" runat="server" 
                            OnClick="linkbutton_check_conflict_Click" Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black">Check Whether Courses Can Conflict</asp:LinkButton>
                    </td>
                    </td>
                    <td>
                    
                    </td>
                         
                </tr>
                 <tr>
                    <td class="style3">
                        &nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small1" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
        <asp:LinkButton ID="linkbutton_general_conflict_check" runat="server"  Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black" onclick="linkbutton_general_conflict_check_Click" 
                            >Find out which set of courses can conflict</asp:LinkButton></td>
                    </td>
                    <td>
                     
                    </td>
                         
                </tr>
               <tr>
                    <td class="style3">
                        &nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small1" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
        <asp:LinkButton ID="linkbutton_create_new_academic_blocks" runat="server"  Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black" 
                            onclick="linkbutton_create_new_academic_blocks_Click">Create Next Term's Academic Blocks</asp:LinkButton></td>
                    </td>
                    <td>
                    
                    </td>
                         
                </tr>
                
               <tr>
                    <td class="style3">
                        &nbsp;&nbsp;&nbsp; <a class="submenulinktext2 " href="" 
                            onblur="window.status=''; return true" 
                            onfocus="window.status='Student Records'; return true" 
                            onmouseout="window.status=''; return true" 
                            onmouseover="window.status='Student Records'; return true">
                        <img border="0" class="headerImg" height="12" hspace="0" 
                            name="sabanci_bullet_red_small2" 
                            src="https://suisimg.sabanciuniv.edu/images/sabanci_bullet_red_small.gif" 
                            title="" vspace="0" width="4" /></a></td>
                    <td>
        <asp:LinkButton ID="linkbutton_upload_file" runat="server"  Font-Bold="True" 
                            Font-Underline="False" ForeColor="Black" onclick="linkbutton_upload_file_Click" 
                            >Upload a File</asp:LinkButton></td>
                    <td>
                    
                        &nbsp;</td>
                         
                </tr>
                
            </table>
        </p>
        <p>
            &nbsp;</p>
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style3
        {
            width: 22px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">
        
            <p>
            <img src="Pictures/sabanci_main_menu.gif" alt="" style="height: 35px; width: 200px" />
            </p>
        
        </asp:Content>


